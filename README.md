# SPZS

## Install dependecies

```bash
yarn install
```

## Setup database

Start a local postgres database and set config in `.env`:

```bash
DATABASE_URL=postgres://user:password@localhost:5432/database
```

Separate database is used for testing, so set config in `.env.test`:

```bash
DATABASE_URL=postgres://user:password@localhost:5432/database_test
```

### Run migrations:

```bash
npx prisma migrate dev
```

## Run server

```bash
yarn build
yarn start
```

## Testing

Before running tests, make sure you have a separate database for testing and set config in `.env.test`. First build the server and run it in test mode:

```bash
yarn build
yarn test:server
```

Then run tests with:

```bash
yarn test:e2e
yarn test:jest
```

## Development

```bash
yarn dev
```

## Usage

Create user on [http://localhost:3000/signup](http://localhost:3000/signup) and login on [http://localhost:3000/login](http://localhost:3000/login).

### Default users

| Email              | Password | Role      |
| ------------------ | -------- | --------- |
| developer@mail.com | password | developer |
| teamlead@mail.com  | password | teamlead  |
