describe("Home component", () => {
  beforeEach(() => {
    // Visit the login page
    cy.viewport(1920, 1080);
    cy.visit("http://localhost:3000/login");

    // Log in
    cy.get("input[name=email]").type("teamlead@mail.com");
    cy.get("input[name=password]").type("password");
    cy.get("button").contains("Login").click();
    cy.get("button").contains("Login").click();

    // Wait for the page to load
    cy.contains("Logs", {
      timeout: 10000,
    }).should("be.visible");

    cy.intercept("/api/trpc/*", {
      fixture: "home.json",
    }).as("getData");
  });

  it("displays meetings on the calendar", () => {
    // Wait for the meetings to load
    cy.wait("@getData");

    // Check that the calendar is visible
    cy.get(".fc").should("be.visible");

    // Check that there are meetings on the calendar
    cy.get(".fc-event").should("exist");

    // Check that the meetings have the correct titles
    cy.get(".fc-event").should("contain", "Meeting 1");
  });

  it("allows creating new meetings", () => {
    cy.wait("@getData");

    // Click on button Create Meeting
    cy.contains("Create meeting").click();

    // Should redirect to /meetings/create
    cy.url().should("include", "/meetings/create");
  });

  it("renders tasklogs", () => {
    cy.wait("@getData");

    // Check that the task logs are visible
    cy.get(".table tbody tr").eq(0).should("contain.text", "Project 1");
    cy.get(".table tbody tr").eq(0).should("contain.text", "Task 2");
    cy.get(".table tbody input").eq(0).should("have.value", "01:10");
  });
});
