describe("Project page", () => {
  beforeEach(() => {
    // Visit the login page
    cy.viewport(1920, 1080);
    cy.visit("http://localhost:3000/login");

    // Log in
    cy.get("input[name=email]").type("teamlead@mail.com");
    cy.get("input[name=password]").type("password");
    cy.get("button").contains("Login").click();
    cy.get("button").contains("Login").click();

    // Wait for the page to load
    cy.contains("Logs", {
      timeout: 10000,
    }).should("be.visible");
    // Visit the project page
    cy.visit("http://localhost:3000/projects/1");
  });

  it("displays the project name", () => {
    cy.contains("Project 1").should("be.visible");
  });

  it("displays the task table", () => {
    // Stub the API request for project data
    cy.intercept("/api/trpc/project.get*", {
      fixture: "project.json",
    });

    // Check that the table is visible
    cy.get(".table").should("be.visible");

    // Check that the table headers are correct
    cy.get(".table th").eq(0).should("contain.text", "#");
    cy.get(".table th").eq(1).should("contain.text", "Name");
    cy.get(".table th").eq(2).should("contain.text", "Status");
    cy.get(".table th").eq(3).should("contain.text", "Assigned");
    cy.get(".table th").eq(4).should("contain.text", "Reported");
    cy.get(".table th").eq(5).should("contain.text", "Last updated");

    // Check that the table rows are correct
    cy.get(".table tbody tr").eq(0).should("contain.text", "1");
    cy.get(".table tbody tr").eq(0).should("contain.text", "Task 1");
    cy.get(".table tbody tr").eq(0).should("contain.text", "In progress");
    cy.get(".table tbody tr").eq(0).should("contain.text", "User 1");
    cy.get(".table tbody tr")
      .eq(0)
      .should("contain.text", "5/2/2023, 6:11:23 PM");

    cy.get(".table tbody tr").eq(1).should("contain.text", "2");
    cy.get(".table tbody tr").eq(1).should("contain.text", "Task 2");
    cy.get(".table tbody tr").eq(1).should("contain.text", "Backlog");
    cy.get(".table tbody tr").eq(1).should("contain.text", "User 2");
    cy.get(".table tbody tr")
      .eq(1)
      .should("contain.text", "5/2/2023, 6:06:59 PM");
  });
});
