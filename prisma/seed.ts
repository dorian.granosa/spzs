import { PrismaClient, UserRole } from "@prisma/client";
import { hash } from "argon2";

const prisma = new PrismaClient();

async function main() {
  const teamlead = await prisma.user.upsert({
    where: { email: "teamlead@mail.com" },

    update: {},

    create: {
      email: "teamlead@mail.com",
      password: await hash("password"),
      firstName: "Mirko",
      lastName: "Markic",
      role: UserRole.TEAM_LEAD,
    },
  });

  const developer = await prisma.user.upsert({
    where: { email: "developer@mail.com" },

    update: {},

    create: {
      email: "developer@mail.com",
      password: await hash("password"),
      firstName: "Ivo",
      lastName: "Ivic",
      role: UserRole.DEVELOPER,
    },
  });

  const project = await prisma.project.upsert({
    where: { name: "Project 1" },

    update: {},

    create: {
      name: "Project 1",
      users: {
        connect: [
          {
            id: teamlead.id,
          },
          {
            id: developer.id,
          },
        ],
      },
    },
  });

  const task1 = await prisma.task.create({
    data: {
      name: "Task 1",
      description: "Task 1 description",
      project: {
        connect: {
          id: project.id,
        },
      },

      assignedTo: {
        connect: {
          id: developer.id,
        },
      },

      reported: {
        connect: {
          id: teamlead.id,
        },
      },
    },
  });

  const task2 = await prisma.task.create({
    data: {
      name: "Task 2",
      description: "Task 2 description",
      project: {
        connect: {
          id: project.id,
        },
      },

      assignedTo: {
        connect: {
          id: teamlead.id,
        },
      },

      reported: {
        connect: {
          id: teamlead.id,
        },
      },
    },
  });

  const meeting = await prisma.meeting.create({
    data: {
      name: "Meeting 1",
      description: "Meeting 1 description",
      startTime: new Date(),
      duration: 60,
      repeatingInterval: 7,

      project: {
        connect: {
          id: project.id,
        },
      },

      attendees: {
        connect: [
          {
            id: teamlead.id,
          },
          {
            id: developer.id,
          },
        ],
      },
    },
  });

  console.log({ teamlead, developer, project, task1, task2, meeting });
}

main()
  .then(async () => {
    await prisma.$disconnect();
  })
  .catch(async (e) => {
    console.error(e);
    await prisma.$disconnect();
    process.exit(1);
  });
