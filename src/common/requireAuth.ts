import type {
  GetServerSidePropsContext,
  GetServerSidePropsResult,
  PreviewData,
} from "next";
import { getServerSession, Session } from "next-auth";
import { ParsedUrlQuery } from "querystring";

import { nextAuthOptions } from "./auth";

export type GetServerSidePropsWithSession<
  Props extends { [key: string]: any } = { [key: string]: any },
  Params extends ParsedUrlQuery = ParsedUrlQuery,
  Preview extends PreviewData = PreviewData
> = (
  context: GetServerSidePropsContext<Params, Preview>,
  session: Session
) => Promise<GetServerSidePropsResult<Props>>;

export const requireAuth =
  (func: GetServerSidePropsWithSession) =>
  async (ctx: GetServerSidePropsContext) => {
    const session = await getServerSession(ctx.req, ctx.res, nextAuthOptions);

    if (!session) {
      return {
        redirect: {
          destination: "/login", // login path
          permanent: false,
        },
      };
    }

    return await func(ctx, session);
  };
