import * as z from "zod";

export const meetingSchema = z.object({
  name: z.string(),
  projectId: z.number().optional(),
  startTime: z.string(),
  duration: z.number(),
  interval: z.number(),
  description: z.string(),
  users: z.array(
    z.object({
      id: z.number(),
      firstName: z.string(),
      lastName: z.string(),
      email: z.string(),
      role: z.string(),
    })
  ),
});

export const meetingGetSchema = z.object({
  id: z.number(),
});

export const meetingSearchSchema = z.object({
  userId: z.number(),
});

export type IMeeting = z.infer<typeof meetingSchema>;
export type IMeetingGet = z.infer<typeof meetingGetSchema>;
export type IMeetingSearch = z.infer<typeof meetingSearchSchema>;
