import * as z from "zod";

export const projectSchema = z.object({
  name: z.string(),
  users: z.array(
    z.object({
      id: z.number(),
      firstName: z.string(),
      lastName: z.string(),
      email: z.string(),
      role: z.string(),
    })
  ),
});

export const projectEditSchema = z.object({
  id: z.number(),
  name: z.string(),
  users: z.array(
    z.object({
      id: z.number(),
      firstName: z.string(),
      lastName: z.string(),
      email: z.string(),
      role: z.string(),
    })
  ),
});

export const projectGetSchema = z.object({
  id: z.number(),
  userFilter: z.string().optional(),
  userRoleFilter: z.string().optional(),
  taskFilter: z.string().optional(),
  taskStatusFilter: z.string().optional(),
});

export const projectSearchSchema = z.object({
  name: z.string(),
});

export type IProject = z.infer<typeof projectSchema>;
export type IProjectGet = z.infer<typeof projectGetSchema>;
export type IProjectSearch = z.infer<typeof projectSearchSchema>;
