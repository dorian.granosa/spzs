import * as z from "zod";

export const taskSchema = z.object({
  id: z.number(),
  name: z.string(),
  description: z.string(),
  status: z.string(),
  assignedTo: z.number(),
  reported: z.number(),
  project: z.number(),
});

export const taskCreateSchema = z.object({
  name: z.string(),
  description: z.string(),
  status: z.string(),
  assignedTo: z.number(),
  reported: z.number(),
  project: z.number(),
});

export const getUserTasksSchema = z.object({
  projectId: z.number(),
});

export type ITask = z.infer<typeof taskSchema>;
export type ITaskCreate = z.infer<typeof taskCreateSchema>;
export type IGetUserTasks = z.infer<typeof getUserTasksSchema>;
