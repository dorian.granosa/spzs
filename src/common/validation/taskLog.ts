import * as z from "zod";

export const taskLogSchema = z.object({
  date: z.string(),
  taskId: z.number(),
  duration: z.number(),
});

export const taskLogGetSchema = z.object({
  date: z.string(),
});

export type ITaskLog = z.infer<typeof taskLogSchema>;
export type ITaskLogGet = z.infer<typeof taskLogGetSchema>;
