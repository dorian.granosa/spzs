import * as z from "zod";

export const userGetSchema = z.object({
  id: z.number(),
  taskFilter: z.string().optional(),
  taskStatusFilter: z.string().optional(),
  projectFilter: z.string().optional(),
});

export const userSearchSchema = z.object({
  filter: z.string(),
  role: z.string().optional(),
});

export type IUserGet = z.infer<typeof userGetSchema>;
export type IUserSearch = z.infer<typeof userSearchSchema>;
