import { User } from "next-auth";
import React, { FC } from "react";
import { SideNav } from "./SideNav";
import { TopNav } from "./TopNav";

interface NavProps {
  currentPath: string;
  user: User | undefined;
  children: React.ReactNode;
}

export const Nav: FC<NavProps> = ({ currentPath, user, children }) => {
  return (
    <div className="drawer">
      <input id="my-drawer" type="checkbox" className="drawer-toggle" />

      <div className="drawer-content">
        <div className="flex flex-col md:flex-row">
          <SideNav currentPath={currentPath} user={user} />
          <div className="flex-1 h-screen overflow-y-auto">
            <TopNav user={user} />
            <main className="flex-1">{children}</main>
          </div>
        </div>
      </div>

      <div className="drawer-side">
        <label htmlFor="my-drawer" className="drawer-overlay"></label>
        <SideNav currentPath={currentPath} user={user} inDrawer={true} />
      </div>
    </div>
  );
};
