import { Task } from "@prisma/client";
import React, { FC } from "react";

interface ProjectTaskBadgesProps {
  tasks:
    | {
        status: Task["status"];
      }[]
    | undefined;
}

export const ProjectTaskBadges: FC<ProjectTaskBadgesProps> = ({ tasks }) => {
  return (
    <div className="flex flex-row gap-1">
      <div className="tooltip" data-tip="Backlog">
        <div className="badge text-black bg-bright-pink">
          {tasks?.filter((task) => task.status === "BACKLOG").length ?? 0}
        </div>
      </div>
      <div className="tooltip" data-tip="Todo">
        <div className="badge text-black bg-lavender-purple">
          {tasks?.filter((task) => task.status === "TODO").length ?? 0}
        </div>
      </div>
      <div className="tooltip" data-tip="In progress">
        <div className="badge text-black bg-baby-blue">
          {tasks?.filter((task) => task.status === "IN_PROGRESS").length ?? 0}
        </div>
      </div>
      <div className="tooltip" data-tip="Testing">
        <div className="badge text-black bg-pale-yellow">
          {tasks?.filter((task) => task.status === "TESTING").length ?? 0}
        </div>
      </div>
      <div className="tooltip" data-tip="Done">
        <div className="badge text-black bg-mint-green">
          {tasks?.filter((task) => task.status === "DONE").length ?? 0}
        </div>
      </div>
    </div>
  );
};
