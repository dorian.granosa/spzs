import { MagnifyingGlassIcon } from "@heroicons/react/24/outline";
import React, { FC } from "react";
import { Spinner } from "./Spinner";

interface SearchProps {
  setCallback: (value: string) => void;
  isLoading: boolean;
}

export const Search: FC<SearchProps> = ({ setCallback, isLoading }) => {
  return (
    <div className="input-group w-auto">
      <input
        type="text"
        placeholder="Search…"
        className="input input-bordered"
        onChange={(e) => {
          setCallback(e.target.value);
        }}
      />
      <button className="btn btn-square">
        {isLoading ? <Spinner /> : <MagnifyingGlassIcon className="w-6 h-6" />}
      </button>
    </div>
  );
};
