import {
  ArrowLeftOnRectangleIcon,
  BriefcaseIcon,
  HomeIcon,
  UserIcon,
} from "@heroicons/react/24/outline";
import { User } from "next-auth";
import { signOut } from "next-auth/react";
import Link from "next/link";
import React, { FC } from "react";
import Avatar, { genConfig } from "react-nice-avatar";
import { ThemeSelector } from "./ThemeSelector";

interface SideNavProps {
  currentPath: string;
  user: User | undefined;
  inDrawer?: boolean;
}

export const SideNav: FC<SideNavProps> = ({ currentPath, user, inDrawer }) => {
  return (
    <nav
      className={
        !inDrawer
          ? "w-64 bg-base-300 hidden md:flex flex-col "
          : "w-80 bg-base-300 flex flex-col"
      }
    >
      {
        // Navigation
      }
      <div className="flex items-center justify-center h-16">
        <Link className="font-semibold text-2xl tracking-tight" href={"/"}>
          SPZS
        </Link>
      </div>
      <div className="flex items-center justify-center card shadow-xl w-11/12 mx-auto bg-accent py-3">
        <div className="flex flex-col items-center">
          <div className="relative w-20 h-20">
            {user?.email && (
              <Avatar
                className="w-20 h-20 rounded-full object-cover ring ring-accent ring-offset-base-100 ring-offset-2"
                {...genConfig(user?.email)}
              />
            )}
          </div>
          <Link href={`/users/${user?.id}`}>
            <p className="mt-2 font-medium text-accent-content">{`${user?.firstName} ${user?.lastName}`}</p>
          </Link>
        </div>
      </div>
      <div className="mt-3">
        <ul className="menu p-4 w-full text-base-content gap-2">
          <li>
            <Link
              href="/"
              className={currentPath == "dashboard" ? "active" : ""}
            >
              <HomeIcon className="h-6 w-6" />
              <span className="mx-4">Dashboard</span>
            </Link>
          </li>
          <li>
            <Link
              href="/projects"
              className={currentPath == "projects" ? "active" : ""}
            >
              <BriefcaseIcon className="h-6 w-6" />
              <span className="mx-4">Projects</span>
            </Link>
          </li>
          <li>
            <Link
              href="/users"
              className={currentPath == "users" ? "active" : ""}
            >
              <UserIcon className="h-6 w-6" />
              <span className="mx-4">Users</span>
            </Link>
          </li>
          <li></li>
          <li>
            <button onClick={() => signOut()}>
              <ArrowLeftOnRectangleIcon className="h-6 w-6" />
              <span className="mx-4">Logout</span>
            </button>
          </li>
        </ul>
      </div>

      <div className="flex-grow"></div>
      <div className="m-4">
        <ThemeSelector />
      </div>
    </nav>
  );
};
