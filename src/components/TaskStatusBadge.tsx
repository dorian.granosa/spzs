import { Task } from "@prisma/client";
import React, { FC } from "react";

interface TaskStatusBadgeProps {
  status: Task["status"];
}

export const TaskStatusBadge: FC<TaskStatusBadgeProps> = ({ status }) => {
  switch (status) {
    case "BACKLOG":
      return (
        <div className="badge badge-lg text-black bg-bright-pink">Backlog</div>
      );
    case "TODO":
      return (
        <div className="badge badge-lg text-black bg-lavender-purple">Todo</div>
      );
    case "IN_PROGRESS":
      return (
        <div className="badge badge-lg text-black bg-baby-blue">
          In progress
        </div>
      );
    case "TESTING":
      return (
        <div className="badge badge-lg text-black bg-pale-yellow">Testing</div>
      );
    case "DONE":
      return (
        <div className="badge badge-lg text-black bg-mint-green">Done</div>
      );
  }
};
