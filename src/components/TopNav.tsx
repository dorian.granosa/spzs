import { User } from "next-auth";
import Link from "next/link";
import React, { FC } from "react";
import { ThemeSelector } from "./ThemeSelector";

interface TopNavProps {
  user: User | undefined;
}

export const TopNav: FC<TopNavProps> = ({ user }) => {
  return (
    <div className="navbar bg-base-300 md:hidden flex flex-row">
      <div className="flex-none">
        <label
          htmlFor="my-drawer"
          className="btn btn-square btn-ghost drawer-button"
        >
          <svg
            xmlns="http://www.w3.org/2000/svg"
            fill="none"
            viewBox="0 0 24 24"
            className="inline-block w-5 h-5 stroke-current"
          >
            <path
              strokeLinecap="round"
              strokeLinejoin="round"
              strokeWidth="2"
              d="M4 6h16M4 12h16M4 18h16"
            ></path>
          </svg>
        </label>
      </div>
      <div className="flex-1">
        <Link className="btn btn-ghost normal-case text-xl" href="/">
          SPZS
        </Link>
      </div>
      <div className="flex-none mr-3">
        <ThemeSelector />
      </div>
      <div className="flex-none">
        <p className="text-xl pr-2">{`${user?.firstName} ${user?.lastName}`}</p>
        <label tabIndex={0} className="btn btn-ghost btn-circle avatar">
          <div className="w-10 rounded-full">
            <img
              src={
                user?.role == "DEVELOPER"
                  ? "/developer-avatar.png"
                  : "/teamlead-avatar.png"
              }
              alt={user?.role}
            />
          </div>
        </label>
      </div>
    </div>
  );
};
