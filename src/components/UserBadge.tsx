import { XMarkIcon } from "@heroicons/react/24/outline";
import { User } from "@prisma/client";
import { FC } from "react";
import Avatar, { genConfig } from "react-nice-avatar";

interface UserBadgeProps {
  user: User;
  onClick?: (e: any) => void;
  showX?: boolean;
}

export const UserBadge: FC<UserBadgeProps> = ({ user, onClick, showX }) => {
  return (
    <div className="justify-start btn h-14 w-72" onClick={onClick}>
      <div className="flex items-center">
        <Avatar className="w-10 h-10" {...genConfig(user?.email)} />
        <div className="ml-2">
          <div className="text-lg font-bold text-left text-secondary">
            {user?.firstName + " " + user?.lastName}
          </div>
          <div className="text-sm text-left text-gray-500">{user?.email}</div>
        </div>
      </div>
      {showX && <XMarkIcon className="w-5 ml-auto" />}
    </div>
  );
};
