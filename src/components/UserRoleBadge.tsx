import { User } from "@prisma/client";
import React, { FC } from "react";

interface UserRoleBadgeProps {
  role: User["role"];
}

export const UserRoleBadge: FC<UserRoleBadgeProps> = ({ role }) => {
  switch (role) {
    case "DEVELOPER":
      return <div className="badge text-black bg-mint-green">Developer</div>;
    case "TEAM_LEAD":
      return <div className="badge text-black bg-baby-blue">Team lead</div>;
  }
};
