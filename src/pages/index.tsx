import moment from "moment";
import type { NextPage } from "next";
import { useSession } from "next-auth/react";

import { requireAuth } from "../common/requireAuth";
import { Nav } from "../components";

import { trpc } from "@/common/client/trpc";
import FullCalendar from "@fullcalendar/react";
import timeGridPlugin from "@fullcalendar/timegrid";
import { ArrowLeftIcon, ArrowRightIcon } from "@heroicons/react/24/outline";
import { DatePicker, LocalizationProvider } from "@mui/x-date-pickers";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import { Meeting, Project, Task } from "@prisma/client";
import dayjs from "dayjs";
import Link from "next/link";
import { createRef, useEffect, useState } from "react";

export const getServerSideProps = requireAuth(async (ctx) => {
  return { props: {} };
});

const Home: NextPage = () => {
  const { data } = useSession();
  const calendarRef = createRef<FullCalendar>();

  const [meetings, setMeetings] = useState<any[]>([]);

  const [selectedDate, setSelectedDate] = useState(dayjs());

  const [projectFilter, setProjectFilter] = useState("");
  const [taskLogProject, setTaskLogProject] = useState<Project | undefined>();

  const [taskFilter, setTaskFilter] = useState("");
  const [taskLogTask, setTaskLogTask] = useState<Task | undefined>();

  const [duration, setDuration] = useState("");

  const eventColors = ["#e91e63", "#9c27b0", "#673ab7", "#3f51b5", "#2196f3"];

  const query = trpc.meeting.search.useQuery(
    { userId: data?.user.id || 1 },
    {
      enabled: !!data,
    }
  );
  const projectQuery = trpc.project.search.useQuery(
    { name: projectFilter },
    {
      enabled: !!data,
    }
  );
  const tasksQuery = trpc.task.getUserTasks.useQuery(
    { projectId: Number(taskLogProject?.id) },
    {
      enabled: !!data && !!taskLogProject,
    }
  );
  const taskLogsQuery = trpc.taskLog.get.useQuery(
    {
      date: selectedDate.toISOString(),
    },
    {
      enabled: !!data,
    }
  );

  const taskLogUpdateMutation = trpc.taskLog.update.useMutation();
  const taskLogDeleteMutation = trpc.taskLog.delete.useMutation();

  useEffect(() => {
    if (data) {
      query.refetch();
      taskLogsQuery.refetch();
    }
  }, [data]);

  useEffect(() => {
    if (query.isFetched && query.data?.result) {
      setMeetings(
        parseMeetings(JSON.parse(JSON.stringify(query.data?.result)))
      );
    }
  }, [query.isFetched]);

  const parseMeetings = (meetings: Meeting[]) => {
    return meetings.map((meeting) => {
      if (meeting.repeatingInterval > 0) {
        return {
          id: meeting.id,
          title: meeting.name,
          url: `/meetings/${meeting.id}`,
          startTime: moment(meeting.startTime).format("HH:mm:ss"),
          endTime: moment(meeting.startTime)
            .add(meeting.duration, "minutes")
            .format("HH:mm:ss"),
          startRecur: moment(meeting.startTime).format("YYYY-MM-DD"),
          daysOfWeek: [moment(meeting.startTime).day()],
          color: eventColors[meeting.projectId ?? 0 % eventColors.length],
        };
      }

      return {
        id: meeting.id,
        title: meeting.name,
        url: `/meetings/${meeting.id}`,
        start: meeting.startTime,
        end: moment(meeting.startTime).add(meeting.duration, "minutes"),
        color: eventColors[meeting.projectId ?? 0 % eventColors.length],
      };
    });
  };

  return (
    <Nav currentPath="dashboard" user={data?.user}>
      <div
        className="m-3"
        style={
          {
            "--fc-page-bg-color": "hsl(var(--p) / 1)",
          } as React.CSSProperties
        }
      >
        <style jsx global>{`
          a.fc-col-header-cell-cushion {
            color: hsl(var(--pc) / 1);
          }
        `}</style>

        {/* TaskLog */}
        <div className="flex flex-row mb-4">
          <h1 className="text-4xl font-bold text-secondary">Logs:</h1>
          <div className="flex-grow"></div>
          <div
            className="flex flex-row items-center justify-center cursor-pointer w-14 h-14"
            onClick={() => {
              setSelectedDate(selectedDate.subtract(1, "day"));
              calendarRef.current?.getApi().gotoDate(selectedDate.toDate());
            }}
          >
            <ArrowLeftIcon className="h-6" />
          </div>
          <LocalizationProvider dateAdapter={AdapterDayjs}>
            <DatePicker
              sx={{
                "& .MuiFormLabel-root": { color: "rgb(156 163 175 / 1)" },
                "& .MuiInputBase-input": { color: "rgb(156 163 175 / 1)" },
                "& .MuiSvgIcon-root": { color: "rgb(156 163 175 / 1)" },
                "& .MuiOutlinedInput-notchedOutline": {
                  borderColor: "rgb(156 163 175 / 0.2)",
                },
              }}
              className="w-48"
              value={selectedDate}
              onChange={(e) => {
                if (e) {
                  setSelectedDate(e);
                  calendarRef.current?.getApi().gotoDate(e.toDate());
                }
              }}
            />
          </LocalizationProvider>
          <div
            className="flex flex-row items-center justify-center cursor-pointer w-14 h-14"
            onClick={() => {
              setSelectedDate(selectedDate.add(1, "day"));
              calendarRef.current?.getApi().gotoDate(selectedDate.toDate());
            }}
          >
            <ArrowRightIcon className="h-6" />
          </div>
        </div>

        <table className="table w-full">
          <thead>
            <tr>
              <th>Project</th>
              <th>Task</th>
              <th>Duration</th>
            </tr>
          </thead>
          <tbody>
            {taskLogsQuery.data?.map((t) => {
              return (
                <tr key={t.id}>
                  <td>
                    <Link href={`/projects/${t.task.projectId}`}>
                      {t.task.project.name}
                    </Link>
                  </td>
                  <td>
                    <Link href={`/tasks/${t.taskId}`}>{t.task.name}</Link>
                  </td>
                  <td>
                    <input
                      type="text"
                      className="input input-bordered h-14 w-72"
                      defaultValue={
                        `${Math.floor(t.duration / 60)}`.padStart(2, "0") +
                        ":" +
                        `${t.duration % 60}`.padStart(2, "0")
                      }
                      onBlur={async (e) => {
                        // parse format HH:MM to minutes number
                        const [hours, minutes] = e.target.value.split(":");
                        const duration = Number(hours) * 60 + Number(minutes);

                        if (isNaN(duration)) return;

                        if (duration > 0) {
                          await taskLogUpdateMutation.mutateAsync({
                            duration,
                            date: selectedDate.toISOString(),
                            taskId: t.taskId,
                          });
                        } else {
                          await taskLogDeleteMutation.mutateAsync({
                            date: selectedDate.toISOString(),
                            taskId: t.taskId,
                            duration,
                          });
                        }

                        taskLogsQuery.refetch();
                      }}
                    />
                  </td>
                </tr>
              );
            })}
            <tr>
              <td>
                <div className="dropdown">
                  <input
                    type="text"
                    placeholder="Find a project"
                    className="input input-bordered h-14 w-72"
                    value={projectFilter}
                    onChange={(e) => {
                      setProjectFilter(e.target.value);
                    }}
                  />
                  <ul
                    tabIndex={0}
                    className="p-2 shadow dropdown-content menu bg-base-300 rounded-box w-72"
                  >
                    {projectQuery.data?.result.slice(0, 3).map((p) => {
                      return (
                        <li key={p.name} className="w-full">
                          <a
                            className="w-full"
                            onClick={() => {
                              // @ts-ignore
                              document.activeElement?.blur();
                              setProjectFilter(p.name);
                              setTaskLogProject(JSON.parse(JSON.stringify(p)));
                            }}
                          >
                            <h1 className="overflow-hidden text-ellipsis">
                              {p.name}
                            </h1>
                          </a>
                        </li>
                      );
                    })}
                  </ul>
                </div>
              </td>
              <td>
                <div className="dropdown">
                  <input
                    type="text"
                    placeholder="Find a task"
                    className="input input-bordered h-14 w-72"
                    value={taskFilter}
                    onChange={(e) => {
                      setTaskFilter(e.target.value);
                    }}
                  />
                  <ul
                    tabIndex={0}
                    className="p-2 shadow dropdown-content menu bg-base-300 rounded-box w-72"
                  >
                    {tasksQuery.data?.slice(0, 3).map((t) => {
                      return (
                        <li key={t.name} className="w-full">
                          <a
                            className="w-full"
                            onClick={() => {
                              // @ts-ignore
                              document.activeElement?.blur();
                              setTaskFilter(t.name);
                              setTaskLogTask(JSON.parse(JSON.stringify(t)));
                            }}
                          >
                            {/* h1 t.name width of parent with line clamp 1 */}
                            <h1 className="overflow-hidden text-ellipsis">
                              {t.name}
                            </h1>
                          </a>
                        </li>
                      );
                    })}
                  </ul>
                </div>
              </td>
              <td>
                {/* input duration in format 00:00 */}
                <input
                  type="text"
                  placeholder="00:00"
                  className="input input-bordered h-14 w-72"
                  value={duration}
                  onChange={(e) => {
                    setDuration(e.target.value);
                  }}
                  onBlur={async (e) => {
                    // parse format HH:MM to minutes number
                    const [hours, minutes] = e.target.value.split(":");
                    const duration = Number(hours) * 60 + Number(minutes);

                    if (!duration) return;

                    if (taskLogTask?.id == undefined) return;

                    if (duration > 0) {
                      await taskLogUpdateMutation.mutateAsync({
                        duration,
                        date: selectedDate.toISOString(),
                        taskId: taskLogTask?.id,
                      });
                    } else {
                      await taskLogDeleteMutation.mutateAsync({
                        date: selectedDate.toISOString(),
                        taskId: taskLogTask?.id,
                        duration,
                      });
                    }

                    taskLogsQuery.refetch();

                    setDuration("");
                    setTaskFilter("");
                    setProjectFilter("");
                    setTaskLogTask(undefined);
                    setTaskLogProject(undefined);
                  }}
                />
              </td>
            </tr>
          </tbody>
        </table>

        <div className="divider"></div>

        {/* Calendar */}
        <div className="flex flex-row mb-4">
          <h1 className="text-4xl font-bold text-secondary">Meetings:</h1>
          <div className="flex-grow"></div>
          <button className="btn btn-primary">
            <Link href="/meetings/create">Create meeting</Link>
          </button>
        </div>

        <FullCalendar
          plugins={[timeGridPlugin]}
          allDaySlot={false}
          initialView="timeGridWeek"
          weekends={false}
          slotMinTime={"07:00:00"}
          slotMaxTime={"17:00:00"}
          nowIndicator={true}
          events={meetings}
          height="auto"
          ref={calendarRef}
        />
      </div>
    </Nav>
  );
};

export default Home;
