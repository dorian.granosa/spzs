import type { InferGetServerSidePropsType, NextPage } from "next";
import { requireAuth } from "../../common/requireAuth";

import { User } from "@prisma/client";
import Link from "next/link";
import { Nav, UserBadge } from "../../components";

import prisma from "@/common/prisma";
import moment from "moment";

export const getServerSideProps = requireAuth(async (ctx, session) => {
  if (
    !ctx.params?.id ||
    isNaN(Number(ctx.params?.id)) ||
    Number(ctx.params?.id) < 0
  )
    return { notFound: true };

  const meeting = await prisma.meeting?.findUnique({
    where: {
      id: Number(ctx.params?.id),
    },
    include: {
      attendees: true,
      project: true,
      _count: {
        select: {
          attendees: true,
        },
      },
    },
  });

  if (!meeting) return { notFound: true };

  return {
    props: {
      meeting: JSON.parse(JSON.stringify(meeting)),
      session,
    },
  };
});

const Meetings: NextPage = ({
  meeting,
  session,
}: InferGetServerSidePropsType<typeof getServerSideProps>) => {
  return (
    <Nav currentPath="projects" user={session?.user}>
      <div className="flex flex-col w-auto m-3">
        <div className="flex flex-row">
          <h1 className="w-full mb-4 text-6xl font-bold leading-snug underline text-secondary">
            <Link href={`/meetings/${meeting?.id}`}>{meeting?.name}</Link>
          </h1>
        </div>

        {/* Project */}
        {meeting?.project && (
          <div className="flex flex-row gap-2">
            <h1 className="mb-4 text-4xl font-bold leading-snug text-secondary">
              Project:
            </h1>
            <Link
              href={`/projects/${meeting?.projectId}`}
              className="mb-4 text-4xl font-bold leading-snug text-secondary"
            >
              {meeting?.project?.name}
            </Link>
          </div>
        )}

        {/* Start Time */}
        <h1 className="mb-4 text-4xl font-bold leading-snug text-secondary">
          {moment(meeting?.startTime).format("MMMM Do YYYY, h:mm a")} (
          {meeting?.duration} minutes)
        </h1>

        {/* Description */}
        <p className="p-1 text-xl whitespace-pre-wrap min-h-80">
          {meeting?.description}
        </p>

        {/* Users */}
        <h1 className="mb-4 text-4xl font-bold leading-snug text-secondary">
          Users ({meeting?._count.attendees}):
        </h1>
        <div className="flex flex-row flex-wrap gap-2 px-1">
          {meeting?.attendees.map((u: User) => {
            return <UserBadge key={u.id} user={u} />;
          })}
        </div>
      </div>
    </Nav>
  );
};

export default Meetings;
