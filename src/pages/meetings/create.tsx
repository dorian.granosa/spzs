import type { InferGetServerSidePropsType, NextPage } from "next";
import { requireAuth } from "../../common/requireAuth";

import { Nav, UserBadge } from "../../components";

import { trpc } from "@/common/client/trpc";
import { DateTimePicker, LocalizationProvider } from "@mui/x-date-pickers";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import { User } from "@prisma/client";
import { SerializeObject } from "@trpc/server/shared";
import dayjs from "dayjs";
import { useRouter } from "next/router";
import { useState } from "react";
import Avatar, { genConfig } from "react-nice-avatar";

export const getServerSideProps = requireAuth(async (ctx, session) => {
  return {
    props: {
      session,
    },
  };
});

const CreateMeeting: NextPage = ({
  session,
}: InferGetServerSidePropsType<typeof getServerSideProps>) => {
  const router = useRouter();

  const [meetingName, setMeetingName] = useState("");
  const [meetingWeekly, setMeetingWeekly] = useState(false);
  const [meetingProject, setMeetingProject] = useState({ id: 0 });
  const [meetingDuration, setMeetingDuration] = useState(60);
  const [meetingDescription, setMeetingDescription] = useState("");
  const [meetingStartTime, setMeetingStartTime] = useState(dayjs());
  const [meetingUsers, setMeetingUsers] = useState<User[]>(() =>
    session ? [session?.user] : []
  );

  const [meetingNameError, setMeetingNameError] = useState(false);
  const [meetingDurationError, setMeetingDurationError] = useState(false);
  const [meetingDescriptionError, setMeetingDescriptionError] = useState(false);

  const [userFilter, setUserFilter] = useState("");
  const [projectFilter, setProjectFilter] = useState("");

  const userQuery = trpc.user.search.useQuery(
    { filter: userFilter },
    {
      enabled: !!session,
    }
  );
  const projectQuery = trpc.project.search.useQuery(
    { name: projectFilter },
    {
      enabled: !!session,
    }
  );
  const meetingMutation = trpc.meeting.create.useMutation();

  const submit = async () => {
    setMeetingNameError(!meetingName);
    setMeetingDescriptionError(!meetingDescription);

    if (!meetingName || !meetingDescription) return;

    const newMeeting = await meetingMutation.mutateAsync({
      name: meetingName,
      interval: meetingWeekly ? 1 : 0,
      projectId: meetingProject ? meetingProject.id : 0,
      duration: meetingDuration,
      description: meetingDescription,
      users: meetingUsers,
      startTime: meetingStartTime.toISOString(),
    });

    if (newMeeting.status === 201) {
      // @ts-ignore
      router.push(`/meetings/${newMeeting.result.id}`);
    }
  };

  const filterNewUsers = (users: SerializeObject<User>[]) => {
    const newUsers = users.filter((u) => {
      return !meetingUsers.find((pu) => pu.id === u.id);
    });

    return newUsers;
  };

  return (
    <Nav currentPath="projects" user={session?.user}>
      <div className="flex flex-col w-auto gap-4 m-3">
        <div className="flex flex-row gap-2">
          <div className="flex flex-col w-auto">
            <label className="label">
              <span className="text-lg text-gray-400 label-text">Name:</span>
            </label>
            <div className="flex flex-row gap-2 px-1">
              <input
                type="text"
                className="w-full max-w-md text-3xl font-bold input input-bordered"
                value={meetingName}
                onChange={(e) => {
                  setMeetingName(e.target.value);
                  setMeetingNameError(false);
                }}
                required
              />
            </div>
          </div>
          <div className="flex-grow" />
          <button className="btn btn-primary" onClick={() => submit()}>
            Create
          </button>
        </div>

        <div>
          <label className="label">
            <span className="text-lg text-gray-400 label-text">Project:</span>
          </label>
          <div className="px-1 dropdown">
            <input
              type="text"
              placeholder="Type here"
              className="input input-bordered h-14 w-72"
              value={projectFilter}
              onChange={(e) => {
                setProjectFilter(e.target.value);
              }}
            />
            <ul
              tabIndex={0}
              className="p-2 shadow dropdown-content menu bg-base-300 rounded-box w-72"
            >
              {projectQuery.data?.result.slice(0, 3).map((p) => {
                return (
                  <li key={p.name}>
                    <a
                      onClick={() => {
                        // @ts-ignore
                        document.activeElement?.blur();
                        setProjectFilter(p.name);
                        setMeetingProject(JSON.parse(JSON.stringify(p)));
                      }}
                    >
                      <h1>{p.name}</h1>
                    </a>
                  </li>
                );
              })}
            </ul>
          </div>
        </div>

        <div>
          <label className="label">
            <span className="text-lg text-gray-400 label-text">Start:</span>
          </label>
          <LocalizationProvider dateAdapter={AdapterDayjs}>
            <DateTimePicker
              sx={{
                "& .MuiFormLabel-root": { color: "rgb(156 163 175 / 1)" },
                "& .MuiInputBase-input": { color: "rgb(156 163 175 / 1)" },
                "& .MuiSvgIcon-root": { color: "rgb(156 163 175 / 1)" },
                "& .MuiOutlinedInput-notchedOutline": {
                  borderColor: "rgb(156 163 175 / 0.2)",
                },
              }}
              className="px-1 w-72"
              value={meetingStartTime}
              onChange={(e) => {
                if (e) setMeetingStartTime(e);
              }}
            />
          </LocalizationProvider>
        </div>

        <div className="flex flex-col w-auto">
          <label className="label">
            <span className="text-lg text-gray-400 label-text">Duration:</span>
          </label>
          <div className="flex flex-row gap-2 px-1">
            <input
              type="number"
              placeholder="minutes"
              className="w-full max-w-sm input input-bordered"
              value={meetingDuration}
              onChange={(e) => {
                setMeetingDuration(Number(e.target.value));
                setMeetingDurationError(false);
              }}
              required
            />
          </div>
        </div>

        <div className="form-control w-fit">
          <label className="cursor-pointer label">
            <span className="text-lg text-gray-400 label-text">Weekly: </span>
            <input
              type="checkbox"
              className="ml-5 checkbox checkbox-secondary"
              checked={meetingWeekly}
              onChange={() => {
                setMeetingWeekly(!meetingWeekly);
              }}
            />
          </label>
        </div>

        <div>
          <label className="label">
            <span className="text-lg text-gray-400 label-text">
              Description:
            </span>
          </label>
          <div className="px-1">
            <textarea
              className="w-full max-w-lg text-xl input input-bordered h-80"
              value={meetingDescription}
              onChange={(e) => {
                setMeetingDescription(e.target.value);
                setMeetingDescriptionError(false);
              }}
            />
          </div>
        </div>

        <div>
          <label className="label">
            <span className="text-lg text-gray-400 label-text">
              Users ({meetingUsers.length}):
            </span>
          </label>
          <div className="px-1 dropdown">
            <input
              type="text"
              placeholder="Find user..."
              className="input input-bordered h-14 w-72"
              value={userFilter}
              onChange={(e) => setUserFilter(e.target.value)}
            />
            <ul
              tabIndex={0}
              className="p-2 shadow dropdown-content menu bg-base-300 rounded-box w-72"
            >
              {userQuery.data?.result &&
                filterNewUsers(userQuery.data?.result)
                  .slice(0, 3)
                  .map((u) => {
                    return (
                      <li key={u.email}>
                        <div
                          className="flex items-center"
                          onClick={() => {
                            setMeetingUsers([
                              ...meetingUsers,
                              JSON.parse(JSON.stringify(u)),
                            ]);
                            setUserFilter("");
                          }}
                        >
                          <Avatar
                            className="w-10 h-10"
                            {...genConfig(u?.email)}
                          />
                          <div className="ml-2">
                            <div className="text-lg font-bold text-left text-secondary">
                              {u?.firstName + " " + u?.lastName}
                            </div>
                            <div className="text-sm text-left text-gray-500">
                              {u?.email}
                            </div>
                          </div>
                        </div>
                      </li>
                    );
                  })}
            </ul>
          </div>
        </div>

        <div className="flex flex-row flex-wrap gap-2 px-1">
          {meetingUsers.map((u) => {
            return (
              <UserBadge
                key={u.id}
                user={u}
                onClick={() => {
                  setMeetingUsers(meetingUsers.filter((pu) => pu.id !== u.id));
                }}
                showX={true}
              />
            );
          })}
        </div>

        <div className="toast">
          {meetingNameError && (
            <div className="alert alert-error">
              <div>
                <span>Enter meeting name</span>
              </div>
            </div>
          )}

          {meetingDurationError && (
            <div className="alert alert-error">
              <div>
                <span>Enter meeting duration</span>
              </div>
            </div>
          )}

          {meetingDescriptionError && (
            <div className="alert alert-error">
              <div>
                <span>Enter meeting description</span>
              </div>
            </div>
          )}
        </div>
      </div>
    </Nav>
  );
};

export default CreateMeeting;
