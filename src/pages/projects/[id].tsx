import type { InferGetServerSidePropsType, NextPage } from "next";
import { requireAuth } from "../../common/requireAuth";

import {
  BookOpenIcon,
  PencilSquareIcon,
  TrashIcon,
} from "@heroicons/react/24/outline";
import { TaskStatus, UserRole } from "@prisma/client";
import Link from "next/link";
import { useState } from "react";
import { trpc } from "../../common/client/trpc";
import {
  Nav,
  ProjectTaskBadges,
  Search,
  TaskStatusBadge,
  UserRoleBadge,
} from "../../components";

import prisma from "../../common/prisma";

export const getServerSideProps = requireAuth(async (ctx, session) => {
  if (
    !ctx.params?.id ||
    isNaN(Number(ctx.params?.id)) ||
    Number(ctx.params?.id) < 0
  )
    return { notFound: true };

  const project = await prisma.project?.findUnique({
    where: {
      id: Number(ctx.params?.id),
    },
    include: {
      tasks: true,
      _count: {
        select: {
          tasks: true,
          users: true,
        },
      },
    },
  });

  if (!project) return { notFound: true };

  return {
    props: {
      project: JSON.parse(JSON.stringify(project)),
      session,
    },
  };
});

const Project: NextPage = ({
  project,
  session,
}: InferGetServerSidePropsType<typeof getServerSideProps>) => {
  const [userFilter, setUserFilter] = useState("");
  const [userRoleFilter, setUserRoleFilter] = useState<UserRole>();
  const [taskFilter, setTaskFilter] = useState("");
  const [taskStatusFilter, setTaskStatusFilter] = useState<TaskStatus>();

  const query = trpc.project?.get.useQuery(
    {
      id: Number(project?.id),
      userFilter,
      userRoleFilter,
      taskFilter,
      taskStatusFilter,
    },
    {
      enabled: !!session,
    }
  );

  return (
    <Nav currentPath="projects" user={session?.user}>
      <div className="flex flex-col w-auto m-3">
        <div className="flex flex-row">
          <h1 className="w-full mb-4 text-6xl font-bold leading-snug underline text-secondary">
            <Link href={`/projects/${project?.id}`}>{project?.name}</Link>
          </h1>
          {session?.user.role === "TEAM_LEAD" && (
            <button className="btn btn-primary">
              <Link href={`/projects/${project?.id}/edit`}>
                <PencilSquareIcon className="h-5" />
              </Link>
            </button>
          )}
        </div>

        <ProjectTaskBadges tasks={project?.tasks} />

        {/* Tasks */}
        <div className="mb-0 divider"></div>
        <h1 className="mb-4 text-4xl font-bold leading-snug text-secondary">
          Tasks ({project?._count.tasks}):
        </h1>
        <div className="flex flex-row gap-3 mb-5">
          <Search setCallback={setTaskFilter} isLoading={query.isLoading} />
          <select
            id="status"
            className="select select-bordered"
            value={taskStatusFilter}
            defaultValue=""
            onChange={(e) => {
              setTaskStatusFilter(e.target.value as TaskStatus);
            }}
          >
            <option disabled value="">
              Status
            </option>
            {Object.keys(TaskStatus).map((status) => {
              return (
                <option key={status} value={status}>
                  {status}
                </option>
              );
            })}
          </select>
          {taskStatusFilter && (
            <button
              className="btn btn-ghost"
              onClick={() => {
                const select = document.querySelector(
                  "#status"
                ) as HTMLSelectElement;
                select.selectedIndex = 0;
                setTaskStatusFilter(undefined);
              }}
            >
              X
            </button>
          )}
          <div className="flex-grow"></div>
          {session?.user.role === "TEAM_LEAD" && (
            <button className="btn btn-primary">
              <Link
                href={{
                  pathname: "/tasks/create",
                  query: { projectId: project?.id },
                }}
              >
                Create task
              </Link>
            </button>
          )}
        </div>
        <div className="overflow-x-auto">
          <table className="table w-full">
            <thead>
              <tr>
                <th className="w-2 font-bold text-right">#</th>
                <th>Name</th>
                <th>Status</th>
                <th>Assigned</th>
                <th className="hidden lg:table-cell">Reported</th>
                <th className="hidden lg:table-cell">Last updated</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              {query.data?.result &&
                query.data.result.tasks.map((task) => {
                  return (
                    <tr className="hover" key={task.id}>
                      <td className="font-bold text-right">{task.id}</td>
                      <td>
                        <Link href={`/tasks/${task.id}`}>{task.name}</Link>
                      </td>
                      <td>
                        <a
                          onClick={() => {
                            setTaskStatusFilter(task.status);
                          }}
                        >
                          <TaskStatusBadge status={task.status} />
                        </a>
                      </td>
                      <td>
                        <Link href={`/users/${task.assignedTo.id}`}>
                          {`${task.assignedTo.firstName} ${task.assignedTo.lastName}`}
                        </Link>
                      </td>
                      <td className="hidden lg:table-cell">
                        <Link href={`/users/${task.reported.id}`}>
                          {`${task.reported.firstName} ${task.reported.lastName}`}
                        </Link>
                      </td>
                      <td
                        className="hidden lg:table-cell"
                        suppressHydrationWarning
                      >
                        {new Date(task.updatedAt).toLocaleString()}
                      </td>
                      <td className="text-right">
                        <div className="btn-group">
                          <Link
                            href={`/tasks/${task.id}`}
                            className="btn btn-sm"
                          >
                            <BookOpenIcon className="h-4" />
                          </Link>
                          {session?.user?.role === "TEAM_LEAD" && (
                            <Link
                              href={`/tasks/${task.id}/edit`}
                              className="btn btn-sm"
                            >
                              <PencilSquareIcon className="h-4" />
                            </Link>
                          )}
                        </div>
                      </td>
                    </tr>
                  );
                })}
            </tbody>
          </table>
        </div>
        <div className="mb-0 divider"></div>

        {/* Users */}
        <h1 className="mb-4 text-4xl font-bold leading-snug text-secondary">
          Users ({project?._count.users}):
        </h1>
        <div className="flex flex-row gap-3 mb-5">
          <Search setCallback={setUserFilter} isLoading={query.isLoading} />
          <select
            id="role"
            className="select select-bordered"
            value={userRoleFilter}
            defaultValue=""
            onChange={(e) => {
              setUserRoleFilter(e.target.value as UserRole);
            }}
          >
            <option disabled value="">
              Role
            </option>
            {Object.keys(UserRole).map((status) => {
              return (
                <option key={status} value={status}>
                  {status}
                </option>
              );
            })}
          </select>
          {userRoleFilter && (
            <button
              className="btn btn-ghost"
              onClick={() => {
                const select = document.querySelector(
                  "#role"
                ) as HTMLSelectElement;
                select.selectedIndex = 0;
                setUserRoleFilter(undefined);
              }}
            >
              X
            </button>
          )}
          <div className="flex-grow"></div>
          {session?.user.role === "TEAM_LEAD" && (
            <button className="btn btn-primary">
              <Link href={`/projects/${project?.id}/edit`}>Add user</Link>
            </button>
          )}
        </div>
        <div className="overflow-x-auto">
          <table className="table w-full">
            <thead>
              <tr>
                <th className="w-2 font-bold text-right">#</th>
                <th className="hidden lg:table-cell">First name</th>
                <th>Last name</th>
                <th>Email</th>
                <th>Role</th>
                <th className="hidden lg:table-cell">Created</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              {query.data?.result &&
                query.data.result.users.map((user, index) => {
                  return (
                    <tr className="hover" key={user.id}>
                      <td className="font-bold text-right">{index + 1}</td>

                      <td className="hidden lg:table-cell">
                        <Link href={`/users/${user.id}`}>{user.firstName}</Link>
                      </td>
                      <td>
                        <Link href={`/users/${user.id}`}>{user.lastName}</Link>
                      </td>
                      <td>
                        <Link href={`/users/${user.id}`}>{user.email}</Link>
                      </td>
                      <td>
                        <UserRoleBadge role={user.role} />
                      </td>
                      <td
                        className="hidden lg:table-cell"
                        suppressHydrationWarning
                      >
                        {new Date(user.createdAt).toLocaleString()}
                      </td>
                      <td className="text-right">
                        <div className="btn-group">
                          <Link
                            href={`/users/${user.id}`}
                            className="btn btn-sm"
                          >
                            <BookOpenIcon className="h-4" />
                          </Link>
                          {session?.user?.role === "TEAM_LEAD" && (
                            <Link
                              href={`/projects/${project?.id}/removeUser/${user.id}`}
                              className="btn btn-sm"
                            >
                              <TrashIcon className="h-4" />
                            </Link>
                          )}
                        </div>
                      </td>
                    </tr>
                  );
                })}
            </tbody>
          </table>
        </div>
      </div>
    </Nav>
  );
};

export default Project;
