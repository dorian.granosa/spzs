import { requireAuth } from "@/common/requireAuth";
import type { GetServerSideProps, NextPage } from "next";

import prisma from "../../../common/prisma";

export const getServerSideProps: GetServerSideProps = requireAuth(
  async (ctx) => {
    if (
      !ctx.params?.id ||
      isNaN(Number(ctx.params?.id)) ||
      Number(ctx.params?.id) < 0
    )
      return { notFound: true };

    const project = await prisma.project.findUnique({
      where: {
        id: Number(ctx.params?.id),
      },
    });

    if (!project) return { notFound: true };

    // Delete the project and all of its associated tasks
    await prisma.project.delete({
      where: {
        id: Number(ctx.params?.id),
      },
    });

    return {
      redirect: {
        destination: `/projects`,
        permanent: false,
      },
    };
  }
);

const RemoveProject: NextPage = () => {
  return <></>;
};

export default RemoveProject;
