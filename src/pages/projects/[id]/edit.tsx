import { requireAuth } from "@/common/requireAuth";
import type { InferGetServerSidePropsType, NextPage } from "next";

import { Nav, UserBadge } from "@/components";

import { trpc } from "@/common/client/trpc";
import { User } from "@prisma/client";
import { SerializeObject } from "@trpc/server/shared";
import { useRouter } from "next/router";
import { useState } from "react";
import Avatar, { genConfig } from "react-nice-avatar";
import prisma from "../../../common/prisma";

export const getServerSideProps = requireAuth(async (ctx, session) => {
  if (
    !ctx.params?.id ||
    isNaN(Number(ctx.params?.id)) ||
    Number(ctx.params?.id) < 0
  )
    return { notFound: true };

  const project = await prisma.project.findUnique({
    where: {
      id: Number(ctx.params?.id),
    },
    include: {
      users: true,
    },
  });

  if (!project) return { notFound: true };

  return {
    props: {
      project: JSON.parse(JSON.stringify(project)),
      session,
    },
  };
});

const EditProject: NextPage = ({
  project,
  session,
}: InferGetServerSidePropsType<typeof getServerSideProps>) => {
  const router = useRouter();

  const [projectName, setProjectName] = useState(project?.name);
  const [projectUsers, setProjectUsers] = useState<User[]>(project?.users);

  const [projectNameError, setProjectNameError] = useState(false);

  const [userFilter, setUserFilter] = useState("");

  const userQuery = trpc.user.search.useQuery(
    { filter: userFilter },
    {
      enabled: !!session,
    }
  );
  const projectMutation = trpc.project.edit.useMutation();

  const submit = async () => {
    if (!projectName) {
      setProjectNameError(true);
      return;
    }

    const newProject = await projectMutation.mutateAsync({
      id: project.id,
      name: projectName,
      users: projectUsers,
    });

    if (newProject.status === 201) {
      // @ts-ignore
      router.push(`/projects/${newProject.result.id}`);
    }
  };

  const filterNewUsers = (users: SerializeObject<User>[]) => {
    const newUsers = users.filter((u) => {
      return !projectUsers.find((pu) => pu.id === u.id);
    });

    return newUsers;
  };

  return (
    <Nav currentPath="projects" user={session?.user}>
      <div className="flex flex-col w-auto gap-4 m-3">
        <div className="flex flex-row gap-2">
          <div className="flex flex-col w-auto">
            <label className="label">
              <span className="text-lg text-gray-400 label-text">Name:</span>
            </label>
            <div className="flex flex-row gap-2 px-1">
              <input
                type="text"
                className="w-full max-w-md text-3xl font-bold input input-bordered"
                value={projectName}
                onChange={(e) => {
                  setProjectName(e.target.value);
                  setProjectNameError(false);
                }}
                required
              />
            </div>
          </div>
          <div className="flex-grow" />
          <button className="btn btn-primary" onClick={() => submit()}>
            Save
          </button>
        </div>

        <div>
          <label className="label">
            <span className="text-lg text-gray-400 label-text">
              Users ({projectUsers?.length}):
            </span>
          </label>
          <div className="px-1 dropdown">
            <input
              type="text"
              placeholder="Find user..."
              className="input input-bordered h-14 w-72"
              value={userFilter}
              onChange={(e) => setUserFilter(e.target.value)}
            />
            <ul
              tabIndex={0}
              className="p-2 shadow dropdown-content menu bg-base-300 rounded-box w-72"
            >
              {userQuery.data?.result &&
                filterNewUsers(userQuery.data?.result)
                  .slice(0, 3)
                  .map((u) => {
                    return (
                      <li key={u.email}>
                        <div
                          className="flex items-center"
                          onClick={() => {
                            setProjectUsers([
                              ...projectUsers,
                              JSON.parse(JSON.stringify(u)),
                            ]);
                            setUserFilter("");
                          }}
                        >
                          <Avatar
                            className="w-10 h-10"
                            {...genConfig(u?.email)}
                          />
                          <div className="ml-2">
                            <div className="text-lg font-bold text-left text-secondary">
                              {u?.firstName + " " + u?.lastName}
                            </div>
                            <div className="text-sm text-left text-gray-500">
                              {u?.email}
                            </div>
                          </div>
                        </div>
                      </li>
                    );
                  })}
            </ul>
          </div>
        </div>

        <div className="flex flex-row flex-wrap gap-2 px-1">
          {projectUsers?.map((u) => {
            return (
              <UserBadge
                key={u.id}
                user={u}
                onClick={() => {
                  setProjectUsers(projectUsers.filter((pu) => pu.id !== u.id));
                }}
                showX={true}
              />
            );
          })}
        </div>

        <div className="toast">
          {projectNameError && (
            <div className="alert alert-error">
              <div>
                <span>Enter project name</span>
              </div>
            </div>
          )}
        </div>
      </div>
    </Nav>
  );
};

export default EditProject;
