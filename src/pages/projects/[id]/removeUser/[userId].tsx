import { requireAuth } from "@/common/requireAuth";
import type { GetServerSideProps, NextPage } from "next";

import prisma from "../../../../common/prisma";

export const getServerSideProps: GetServerSideProps = requireAuth(
  async (ctx) => {
    if (
      !ctx.params?.id ||
      isNaN(Number(ctx.params?.id)) ||
      Number(ctx.params?.id) < 0
    )
      return { notFound: true };

    const project = await prisma.project.update({
      where: {
        id: Number(ctx.params?.id),
      },
      data: {
        users: {
          disconnect: {
            id: Number(ctx.params?.userId),
          },
        },
      },
    });

    return {
      redirect: {
        destination: `/projects/${ctx.params?.id}`,
        permanent: false,
      },
    };
  }
);

const ProjectRemoveUser: NextPage = () => {
  return <></>;
};

export default ProjectRemoveUser;
