import Link from "next/link";
import type { InferGetServerSidePropsType, NextPage } from "next";
import { useSession } from "next-auth/react";
import { useEffect, useState } from "react";

import {
  BookOpenIcon,
  PencilSquareIcon,
  TrashIcon,
} from "@heroicons/react/24/outline";

import { trpc } from "@/common/client/trpc";
import { requireAuth } from "@/common/requireAuth";
import { Nav, ProjectTaskBadges, Search } from "@/components";

export const getServerSideProps = requireAuth(async (ctx, session) => {
  return { props: { session } };
});

const Projects: NextPage = ({
  session,
}: InferGetServerSidePropsType<typeof getServerSideProps>) => {
  const [projectFilter, setProjectFilter] = useState("");

  const query = trpc.project.search.useQuery(
    { name: projectFilter },
    {
      enabled: !!session,
    }
  );

  useEffect(() => {
    if (session) query.refetch();
  }, [session]);

  return (
    <Nav currentPath="projects" user={session?.user}>
      <main className="m-3">
        <div className="flex flex-row mb-5">
          <Search setCallback={setProjectFilter} isLoading={query.isLoading} />
          <div className="flex-grow"></div>
          {session?.user.role === "TEAM_LEAD" && (
            <button className="btn btn-primary">
              <Link href="/projects/create">Create project</Link>
            </button>
          )}
        </div>
        <div className="overflow-x-auto">
          <table className="table w-full">
            <thead>
              <tr>
                <th className="font-bold text-right w-2">#</th>
                <th>Name</th>
                <th>Developers</th>
                <th>Tasks</th>
                <th className="hidden md:table-cell">Created</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              {query.data?.result.map((project, index) => {
                return (
                  <tr className="hover" key={project.id}>
                    <td className="font-bold text-right">{index + 1}</td>
                    <td>
                      <Link href={`/projects/${project.id}`}>
                        {project.name}
                      </Link>
                    </td>
                    <td>{project.users.length}</td>
                    <td>
                      <ProjectTaskBadges tasks={project.tasks} />
                    </td>
                    <td
                      className="hidden md:table-cell"
                      suppressHydrationWarning
                    >
                      {new Date(project.createdAt).toLocaleString()}
                    </td>
                    <td className="text-right">
                      <div className="btn-group">
                        <Link
                          href={`/projects/${project.id}`}
                          className="btn btn-sm"
                        >
                          <BookOpenIcon className="h-4" />
                        </Link>
                        {session?.user?.role === "TEAM_LEAD" && (
                          <Link
                            href={`/projects/${project.id}/edit`}
                            className="btn btn-sm"
                          >
                            <PencilSquareIcon className="h-4" />
                          </Link>
                        )}

                        {session?.user?.role === "TEAM_LEAD" && (
                          <Link
                            onClick={(e) => {
                              if (
                                !confirm(
                                  `Are you sure you want to delete project: ${project.name}?`
                                )
                              )
                                e.preventDefault();
                            }}
                            href={`/projects/${project.id}/delete`}
                            className="btn btn-sm"
                          >
                            <TrashIcon className="h-4" />
                          </Link>
                        )}
                      </div>
                    </td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        </div>
      </main>
    </Nav>
  );
};

export default Projects;
