import type { NextPage } from "next";
import Head from "next/head";
import Link from "next/link";
import { useCallback, useState } from "react";
import { useRouter } from "next/router";
import { useForm } from "react-hook-form";
import { zodResolver } from "@hookform/resolvers/zod";

import { signUpSchema, ISignUp } from "../common/validation/auth";
import { trpc } from "../common/client/trpc";

const SignUp: NextPage = () => {
  const router = useRouter();
  const [userAlreadyExists, setUserAlreadyExists] = useState(false);
  const { register, handleSubmit } = useForm<ISignUp>({
    resolver: zodResolver(signUpSchema),
  });

  const mutation = trpc.signUp.useMutation();

  const onSubmit = useCallback(
    async (data: ISignUp) => {
      const result: {
        status: number;
      } = await mutation.mutateAsync(data);

      if (result.status === 201) {
        router.push("/");
        return;
      }

      if (result.status == 409) {
        setUserAlreadyExists(true);
        return;
      }
    },
    [mutation, router]
  );

  return (
    <div>
      <Head>
        <title>Next App - Register</title>
        <meta name="description" content="Generated by create next app" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main>
        <form
          className="flex items-center justify-center h-screen w-full"
          onSubmit={handleSubmit(onSubmit)}
        >
          <div className="card w-96 bg-base-100 shadow-xl">
            <div className="card-body">
              <h2 className="card-title">Create an account!</h2>
              <input
                type="text"
                placeholder="Type your first name..."
                className="input input-bordered w-full max-w-xs my-2"
                {...register("firstName")}
              />
              <input
                type="text"
                placeholder="Type your last name..."
                className="input input-bordered w-full max-w-xs my-2"
                {...register("lastName")}
              />
              <input
                type="email"
                placeholder="Type your email..."
                className={`input input-bordered w-full max-w-xs my-2 ${
                  userAlreadyExists ? "input-error" : ""
                }`}
                {...register("email")}
                onChange={() => setUserAlreadyExists(false)}
              />
              <input
                type="password"
                placeholder="Type your password..."
                className="input input-bordered w-full max-w-xs my-2"
                {...register("password")}
              />
              <div className="form-control btn-group flex-row self-center my-2">
                <input
                  type="radio"
                  className="btn"
                  {...register("role")}
                  value={"DEVELOPER"}
                  data-title="Developer"
                  checked
                />
                <input
                  type="radio"
                  className="btn"
                  {...register("role")}
                  value={"TEAM_LEAD"}
                  data-title="Teamlead"
                />
              </div>
              {userAlreadyExists && (
                <div className="text-error">User already exists!</div>
              )}

              <div className="card-actions items-center justify-between">
                <Link href="/login" className="link">
                  Go to login
                </Link>
                <button
                  className="btn btn-secondary"
                  type="submit"
                  disabled={mutation.isLoading}
                >
                  Sign Up
                </button>
              </div>
            </div>
          </div>
        </form>
      </main>
    </div>
  );
};

export default SignUp;
