import type { InferGetServerSidePropsType, NextPage } from "next";
import { requireAuth } from "../../common/requireAuth";

import { Nav, TaskStatusBadge, UserBadge } from "../../components";

import { trpc } from "@/common/client/trpc";
import { CheckIcon, XMarkIcon } from "@heroicons/react/24/outline";
import { TaskLog, TaskStatus } from "@prisma/client";
import Link from "next/link";
import { useState } from "react";
import Avatar, { genConfig } from "react-nice-avatar";
import prisma from "../../common/prisma";

export const getServerSideProps = requireAuth(async (ctx, session) => {
  if (
    !ctx.params?.id ||
    isNaN(Number(ctx.params?.id)) ||
    Number(ctx.params?.id) < 0
  )
    return { notFound: true };

  const task = await prisma.task?.findUnique({
    where: {
      id: Number(ctx.params?.id),
    },
    include: {
      project: true,
      assignedTo: true,
      reported: true,
      taskLogs: true,
    },
  });

  if (!task) return { notFound: true };

  return {
    props: {
      task: JSON.parse(JSON.stringify(task)),
      session,
    },
  };
});

const Task: NextPage = ({
  task,
  session,
}: InferGetServerSidePropsType<typeof getServerSideProps>) => {
  const [editMode, setEditMode] = useState(false);
  const [editAssignedTo, setEditAssignedTo] = useState(false);
  const [editReportedBy, setEditReportedBy] = useState(false);

  const [userFilter, setUserFilter] = useState("");

  const [defaultTaskName, setDefaultTaskName] = useState(task?.name);
  const [defaultTaskDescription, setDefaultTaskDescription] = useState(
    task?.description
  );

  const [taskName, setTaskName] = useState(task?.name);
  const [taskStatus, setTaskStatus] = useState(task?.status);
  const [reportedBy, setReportedBy] = useState(task?.reported);
  const [assignedTo, setAssignedTo] = useState(task?.assignedTo);
  const [taskDescription, setTaskDescription] = useState(task?.description);

  const userQuery = trpc.user.search.useQuery(
    { filter: userFilter },
    {
      enabled: !!session,
    }
  );
  const taskMutation = trpc.task.edit.useMutation();

  const submit = async (props?: any) => {
    const newTask = await taskMutation.mutateAsync({
      id: task?.id,
      name: taskName,
      status: props?.taskStatus ?? taskStatus,
      description: taskDescription,
      assignedTo: props?.assignedTo ?? assignedTo?.id,
      reported: props?.reportedBy ?? reportedBy?.id,
      project: task?.project?.id,
    });

    setTaskName(newTask?.name);
    setTaskStatus(newTask?.status);
    setTaskDescription(newTask?.description);

    setDefaultTaskName(newTask?.name);
    setDefaultTaskDescription(newTask?.description);

    setEditMode(false);
  };

  const cancel = () => {
    setTaskName(defaultTaskName);
    setTaskDescription(defaultTaskDescription);
    setEditMode(false);
  };

  return (
    <Nav currentPath="projects" user={session?.user}>
      <div
        className="flex flex-col w-auto gap-4 m-3"
        onClick={() => {
          setUserFilter("");
          setEditAssignedTo(false);
          setEditReportedBy(false);
        }}
      >
        <div>
          <label className="label">
            <Link href={`/projects/${task?.project?.id}`}>
              <span className="text-gray-400 label-text">ID-{task?.id}</span>
            </Link>
          </label>
          {!editMode ? (
            <h1
              className="w-6/12 text-4xl font-bold"
              onClick={() => setEditMode(true)}
            >
              {taskName}
            </h1>
          ) : (
            <div className="flex flex-row gap-2">
              <input
                type="text"
                className="w-full max-w-md text-3xl font-bold border-white input input-bordered"
                value={taskName}
                onChange={(e) => setTaskName(e.target.value)}
              />
              <button className="btn btn-square btn-outline" onClick={submit}>
                <CheckIcon className="w-6 h-6" />
              </button>
              <button className="btn btn-square btn-outline" onClick={cancel}>
                <XMarkIcon className="w-6 h-6" />
              </button>
            </div>
          )}
        </div>
        <div>
          <label className="label">
            <span className="text-lg text-gray-400 label-text">Status:</span>
          </label>
          <div className="dropdown dropdown-begin">
            <label tabIndex={0} className="px-1 hover:cursor-pointer">
              <TaskStatusBadge status={taskStatus} />
            </label>
            <ul
              tabIndex={0}
              className="w-40 p-2 shadow dropdown-content menu bg-base-300 rounded-box"
            >
              {Object.keys(TaskStatus).map((status) => {
                return (
                  <li key={status}>
                    <a
                      onClick={() => {
                        // @ts-ignore
                        document.activeElement?.blur();
                        setTaskStatus(status as TaskStatus);
                        submit({ taskStatus: status as TaskStatus });
                      }}
                    >
                      <TaskStatusBadge status={status as TaskStatus} />
                    </a>
                  </li>
                );
              })}
            </ul>
          </div>
        </div>

        <div>
          <label className="label">
            <span className="text-lg text-gray-400 label-text">
              Hours Logged:
            </span>
          </label>
          <span className="px-1 text-2xl font-bold">
            {(
              task?.taskLogs.reduce(
                (a: number, b: TaskLog) => a + b.duration,
                0
              ) / 60
            ).toFixed(1)}
          </span>
        </div>

        <div>
          <label className="label">
            <span className="text-lg text-gray-400 label-text">
              Assigned to:
            </span>
          </label>
          {!editAssignedTo ? (
            <UserBadge
              user={assignedTo}
              onClick={(e) => {
                setEditAssignedTo(true);
                e.stopPropagation();
              }}
              showX={true}
            />
          ) : (
            <div className="dropdown dropdown-open">
              <input
                type="text"
                placeholder="Type here"
                className="input input-bordered h-14 w-72"
                autoFocus
                value={userFilter}
                onChange={(e) => setUserFilter(e.target.value)}
              />
              <ul
                tabIndex={0}
                className="p-2 shadow dropdown-content menu bg-base-300 rounded-box w-72"
              >
                {userQuery.data?.result.slice(0, 3).map((u) => {
                  return (
                    <li key={u.email}>
                      <div
                        className="flex items-center"
                        onClick={() => {
                          setAssignedTo(u);
                          setEditAssignedTo(false);
                          submit({ assignedTo: u?.id });
                        }}
                      >
                        <Avatar
                          className="w-10 h-10"
                          {...genConfig(u?.email)}
                        />
                        <div className="ml-2">
                          <div className="text-lg font-bold text-left text-secondary">
                            {u?.firstName + " " + u?.lastName}
                          </div>
                          <div className="text-sm text-left text-gray-500">
                            {u?.email}
                          </div>
                        </div>
                      </div>
                    </li>
                  );
                })}
              </ul>
            </div>
          )}
        </div>

        <div>
          <label className="label">
            <span className="text-lg text-gray-400 label-text">
              Reported by:
            </span>
          </label>
          {!editReportedBy ? (
            <UserBadge
              user={reportedBy}
              onClick={(e) => {
                // setEditReportedBy(true);
                e.stopPropagation();
              }}
            />
          ) : (
            <div className="dropdown dropdown-open">
              <input
                type="text"
                placeholder="Type here"
                className="input input-bordered h-14 w-72"
                autoFocus
                value={userFilter}
                onChange={(e) => setUserFilter(e.target.value)}
              />
              <ul
                tabIndex={0}
                className="p-2 shadow dropdown-content menu bg-base-300 rounded-box w-72"
              >
                {userQuery.data?.result.slice(0, 3).map((u) => {
                  return (
                    <li key={u.email}>
                      <div
                        className="flex items-center"
                        onClick={() => {
                          setReportedBy(u);
                          setEditReportedBy(false);
                          submit({ reportedBy: u?.id });
                        }}
                      >
                        <Avatar
                          className="w-10 h-10"
                          {...genConfig(u?.email)}
                        />
                        <div className="ml-2">
                          <div className="text-lg font-bold text-left text-secondary">
                            {u?.firstName + " " + u?.lastName}
                          </div>
                          <div className="text-sm text-left text-gray-500">
                            {u?.email}
                          </div>
                        </div>
                      </div>
                    </li>
                  );
                })}
              </ul>
            </div>
          )}
        </div>

        <div>
          <label className="label">
            <span className="text-lg text-gray-400 label-text">
              Description:
            </span>
          </label>
          {!editMode ? (
            <p
              className="p-1 text-xl whitespace-pre-wrap h-80"
              onClick={() => setEditMode(true)}
            >
              {taskDescription}
            </p>
          ) : (
            <textarea
              className="w-full max-w-lg text-xl border-white input input-bordered h-80"
              value={taskDescription}
              onChange={(e) => setTaskDescription(e.target.value)}
            />
          )}
        </div>
      </div>
    </Nav>
  );
};

export default Task;
