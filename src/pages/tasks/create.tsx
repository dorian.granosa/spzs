import type { InferGetServerSidePropsType, NextPage } from "next";
import { requireAuth } from "../../common/requireAuth";

import { Nav, TaskStatusBadge, UserBadge } from "../../components";

import { trpc } from "@/common/client/trpc";
import prisma from "@/common/prisma";
import { Project, TaskStatus, User } from "@prisma/client";
import { useRouter } from "next/router";
import { useState } from "react";
import Avatar, { genConfig } from "react-nice-avatar";

export const getServerSideProps = requireAuth(async (ctx, session) => {
  if (!ctx.query.projectId) return { props: { session } };

  const project = await prisma.project.findUnique({
    where: { id: Number(ctx.query.projectId) },
  });

  return {
    props: {
      session,
      project: JSON.parse(JSON.stringify(project)),
    },
  };
});

const CreateTask: NextPage = ({
  session,
  project,
}: InferGetServerSidePropsType<typeof getServerSideProps>) => {
  const router = useRouter();

  const [taskName, setTaskName] = useState("");
  const [taskStatus, setTaskStatus] = useState<TaskStatus>(TaskStatus.BACKLOG);
  const [taskProject, setTaskProject] = useState<Project | undefined>(project);
  const [assignedTo, setAssignedTo] = useState<User | undefined>(session?.user);
  const [taskDescription, setTaskDescription] = useState("");

  const [editAssignedTo, setEditAssignedTo] = useState(false);
  const [taskNameError, setTaskNameError] = useState(false);
  const [taskProjectError, setTaskProjectError] = useState(false);

  const [userFilter, setUserFilter] = useState("");
  const [projectFilter, setProjectFilter] = useState(project?.name ?? "");

  const userQuery = trpc.user.search.useQuery(
    { filter: userFilter },
    {
      enabled: !!session,
    }
  );
  const projectQuery = trpc.project.search.useQuery(
    { name: projectFilter },
    {
      enabled: !!session,
    }
  );
  const taskMutation = trpc.task.create.useMutation();

  const submit = async () => {
    if (!taskName) {
      setTaskNameError(true);
      return;
    }

    if (!taskProject?.id) {
      setTaskProjectError(true);
      return;
    }

    const newTask = await taskMutation.mutateAsync({
      name: taskName,
      status: taskStatus,
      project: taskProject?.id,
      assignedTo: assignedTo?.id ?? session?.user?.id,
      reported: session?.user?.id,
      description: taskDescription,
    });

    if (newTask.id) router.push(`/tasks/${newTask.id}`);
  };

  return (
    <Nav currentPath="projects" user={session?.user}>
      <div className="flex flex-col w-auto gap-4 m-3">
        <div className="flex flex-row gap-2">
          <div className="flex flex-col w-auto">
            <label className="label">
              <span className="text-lg text-gray-400 label-text">Name:</span>
            </label>
            <div className="flex flex-row gap-2">
              <input
                type="text"
                className="w-full max-w-md text-3xl font-bold input input-bordered"
                value={taskName}
                onChange={(e) => {
                  setTaskName(e.target.value);
                  setTaskNameError(false);
                }}
                required
              />
            </div>
          </div>
          <div className="flex-grow" />
          <button className="btn btn-primary" onClick={() => submit()}>
            Create
          </button>
        </div>

        <div>
          <label className="label">
            <span className="text-lg text-gray-400 label-text">Status:</span>
          </label>
          <div className="dropdown dropdown-begin">
            <label tabIndex={0} className="hover:cursor-pointer">
              <TaskStatusBadge status={taskStatus} />
            </label>
            <ul
              tabIndex={0}
              className="w-40 p-2 shadow dropdown-content menu bg-base-300 rounded-box"
            >
              {Object.keys(TaskStatus).map((status) => {
                return (
                  <li key={status}>
                    <a
                      onClick={() => {
                        // @ts-ignore
                        document.activeElement?.blur();
                        setTaskStatus(status as TaskStatus);
                      }}
                    >
                      <TaskStatusBadge status={status as TaskStatus} />
                    </a>
                  </li>
                );
              })}
            </ul>
          </div>
        </div>

        <div>
          <label className="label">
            <span className="text-lg text-gray-400 label-text">Project:</span>
          </label>
          <div className="dropdown">
            <input
              type="text"
              placeholder="Type here"
              className="input input-bordered h-14 w-72"
              value={projectFilter}
              onChange={(e) => {
                setProjectFilter(e.target.value);
                setTaskProjectError(false);
              }}
            />
            <ul
              tabIndex={0}
              className="p-2 shadow dropdown-content menu bg-base-300 rounded-box w-72"
            >
              {projectQuery.data?.result.slice(0, 3).map((p) => {
                return (
                  <li key={p.name}>
                    <a
                      onClick={() => {
                        // @ts-ignore
                        document.activeElement?.blur();
                        setProjectFilter(p.name);
                        setTaskProject(JSON.parse(JSON.stringify(p)));
                      }}
                    >
                      <h1>{p.name}</h1>
                    </a>
                  </li>
                );
              })}
            </ul>
          </div>
        </div>

        <div>
          <label className="label">
            <span className="text-lg text-gray-400 label-text">
              Assigned to:
            </span>
          </label>
          {editAssignedTo || !assignedTo ? (
            <div className="dropdown dropdown-open">
              <input
                type="text"
                placeholder="Type here"
                className="input input-bordered h-14 w-72"
                autoFocus
                value={userFilter}
                onChange={(e) => setUserFilter(e.target.value)}
              />
              <ul
                tabIndex={0}
                className="p-2 shadow dropdown-content menu bg-base-300 rounded-box w-72"
              >
                {userQuery.data?.result.slice(0, 3).map((u) => {
                  return (
                    <li key={u.email}>
                      <div
                        className="flex items-center"
                        onClick={() => {
                          setAssignedTo(JSON.parse(JSON.stringify(u)));
                          setEditAssignedTo(false);
                        }}
                      >
                        <Avatar
                          className="w-10 h-10"
                          {...genConfig(u?.email)}
                        />
                        <div className="ml-2">
                          <div className="text-lg font-bold text-left text-secondary">
                            {u?.firstName + " " + u?.lastName}
                          </div>
                          <div className="text-sm text-left text-gray-500">
                            {u?.email}
                          </div>
                        </div>
                      </div>
                    </li>
                  );
                })}
              </ul>
            </div>
          ) : (
            <UserBadge
              user={assignedTo}
              onClick={(e) => {
                setEditAssignedTo(true);
                userQuery.refetch();
                e.stopPropagation();
              }}
              showX={true}
            />
          )}
        </div>

        <div>
          <label className="label">
            <span className="text-lg text-gray-400 label-text">
              Reported by:
            </span>
          </label>
          <UserBadge user={session?.user} />
        </div>

        <div>
          <label className="label">
            <span className="text-lg text-gray-400 label-text">
              Description:
            </span>
          </label>
          <textarea
            className="w-full max-w-lg text-xl input input-bordered h-80"
            value={taskDescription}
            onChange={(e) => setTaskDescription(e.target.value)}
          />
        </div>
      </div>

      <div className="toast">
        {taskNameError && (
          <div className="alert alert-error">
            <div>
              <span>Enter task name</span>
            </div>
          </div>
        )}

        {taskProjectError && (
          <div className="alert alert-error">
            <div>
              <span>Select project</span>
            </div>
          </div>
        )}
      </div>
    </Nav>
  );
};

export default CreateTask;
