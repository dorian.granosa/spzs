import type { InferGetServerSidePropsType, NextPage } from "next";
import { requireAuth } from "../../common/requireAuth";

import {
  BookOpenIcon,
  PencilSquareIcon,
  TrashIcon,
} from "@heroicons/react/24/outline";
import { TaskStatus } from "@prisma/client";
import Link from "next/link";
import { useState } from "react";
import { trpc } from "../../common/client/trpc";
import {
  Nav,
  ProjectTaskBadges,
  Search,
  TaskStatusBadge,
} from "../../components";

import prisma from "@/common/prisma";

export const getServerSideProps = requireAuth(async (ctx, session) => {
  if (
    !ctx.params?.id ||
    isNaN(Number(ctx.params?.id)) ||
    Number(ctx.params?.id) < 0
  )
    return { notFound: true };

  const user = await prisma.user?.findUnique({
    where: {
      id: Number(ctx.params?.id),
    },
    include: {
      assignedTo: true,
      _count: {
        select: {
          assignedTo: true,
          projects: true,
        },
      },
    },
  });

  if (!user) return { notFound: true };

  return {
    props: {
      user: JSON.parse(JSON.stringify(user)),
      session,
    },
  };
});

const User: NextPage = ({
  user,
  session,
}: InferGetServerSidePropsType<typeof getServerSideProps>) => {
  const [taskFilter, setTaskFilter] = useState("");
  const [taskStatusFilter, setTaskStatusFilter] = useState<TaskStatus>();
  const [projectFilter, setProjectFilter] = useState("");

  const query = trpc.user.get.useQuery(
    {
      id: Number(user?.id),
      taskFilter,
      taskStatusFilter,
      projectFilter,
    },
    {
      enabled: !!session,
    }
  );

  return (
    <Nav currentPath="users" user={session?.user}>
      <div className="flex flex-col w-auto m-3">
        <h1 className="w-full mb-4 text-6xl font-bold leading-snug underline text-secondary">
          <Link
            href={`/users/${user?.id}`}
          >{`${user?.firstName} ${user?.lastName}`}</Link>
        </h1>
        <h1 className="w-full mb-4 text-4xl font-bold leading-snug underline text-baby-blue">
          <Link href={`mailto:${user?.email}`}>{user?.email}</Link>
        </h1>

        <ProjectTaskBadges tasks={user?.assignedTo} />

        {/* Tasks */}
        <div className="mb-0 divider"></div>
        <h1 className="mb-4 text-4xl font-bold leading-snug text-secondary">
          Tasks ({user?._count.assignedTo}):
        </h1>
        <div className="flex flex-row gap-3 mb-5">
          <Search setCallback={setTaskFilter} isLoading={query.isLoading} />
          <select
            id="status"
            className="select select-bordered"
            value={taskStatusFilter}
            defaultValue=""
            onChange={(e) => {
              setTaskStatusFilter(e.target.value as TaskStatus);
            }}
          >
            <option disabled value="">
              Status
            </option>
            {Object.keys(TaskStatus).map((status) => {
              return (
                <option key={status} value={status}>
                  {status}
                </option>
              );
            })}
          </select>
          {taskStatusFilter && (
            <button
              className="btn btn-ghost"
              onClick={() => {
                const select = document.querySelector(
                  "#status"
                ) as HTMLSelectElement;
                select.selectedIndex = 0;
                setTaskStatusFilter(undefined);
              }}
            >
              X
            </button>
          )}
          <div className="flex-grow"></div>
        </div>
        <div className="overflow-x-auto">
          <table className="table w-full">
            <thead>
              <tr>
                <th className="w-2 font-bold text-right">#</th>
                <th>Name</th>
                <th>Status</th>
                <th>Assigned</th>
                <th className="hidden lg:table-cell">Reported</th>
                <th className="hidden lg:table-cell">Last updated</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              {query.data?.result &&
                query.data.result.assignedTo.map((task) => {
                  return (
                    <tr className="hover" key={task.id}>
                      <td className="font-bold text-right">{task.id}</td>
                      <td>
                        <Link href={`/tasks/${task.id}`}>{task.name}</Link>
                      </td>
                      <td>
                        <a
                          onClick={() => {
                            setTaskStatusFilter(task.status);
                          }}
                        >
                          <TaskStatusBadge status={task.status} />
                        </a>
                      </td>
                      <td>
                        <Link href={`/users/${task.assignedTo.id}`}>
                          {`${task.assignedTo.firstName} ${task.assignedTo.lastName}`}
                        </Link>
                      </td>
                      <td className="hidden lg:table-cell">
                        <Link href={`/users/${task.reported.id}`}>
                          {`${task.reported.firstName} ${task.reported.lastName}`}
                        </Link>
                      </td>
                      <td
                        className="hidden lg:table-cell"
                        suppressHydrationWarning
                      >
                        {new Date(task.updatedAt).toLocaleString()}
                      </td>
                      <td className="text-right">
                        <div className="btn-group">
                          <Link
                            href={`/tasks/${task.id}`}
                            className="btn btn-sm"
                          >
                            <BookOpenIcon className="h-4" />
                          </Link>
                          {session?.user?.role === "TEAM_LEAD" && (
                            <Link
                              href={`/tasks/${task.id}/edit`}
                              className="btn btn-sm"
                            >
                              <PencilSquareIcon className="h-4" />
                            </Link>
                          )}
                        </div>
                      </td>
                    </tr>
                  );
                })}
            </tbody>
          </table>
        </div>
        <div className="mb-0 divider"></div>

        {/* Projects */}
        <h1 className="mb-4 text-4xl font-bold leading-snug text-secondary">
          Projects ({user?._count.projects}):
        </h1>
        <div className="flex flex-row mb-5">
          <Search setCallback={setProjectFilter} isLoading={query.isLoading} />
          <div className="flex-grow"></div>
        </div>
        <div className="overflow-x-auto">
          <table className="table w-full">
            <thead>
              <tr>
                <th className="w-2 font-bold text-right">#</th>
                <th>Name</th>
                <th>Developers</th>
                <th>Tasks</th>
                <th className="hidden md:table-cell">Created</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              {query.data?.result &&
                query.data.result.projects.map((project, index) => {
                  return (
                    <tr className="hover" key={project.id}>
                      <td className="font-bold text-right">{index + 1}</td>
                      <td>
                        <Link href={`/projects/${project.id}`}>
                          {project.name}
                        </Link>
                      </td>
                      <td>{project.users.length}</td>
                      <td>
                        <ProjectTaskBadges tasks={project.tasks} />
                      </td>
                      <td
                        className="hidden md:table-cell"
                        suppressHydrationWarning
                      >
                        {new Date(project.createdAt).toLocaleString()}
                      </td>
                      <td className="text-right">
                        <div className="btn-group">
                          <Link
                            href={`/projects/${project.id}`}
                            className="btn btn-sm"
                          >
                            <BookOpenIcon className="h-4" />
                          </Link>
                          {session?.user?.role === "TEAM_LEAD" && (
                            <Link
                              href={`/projects/${project.id}/edit`}
                              className="btn btn-sm"
                            >
                              <PencilSquareIcon className="h-4" />
                            </Link>
                          )}

                          {session?.user?.role === "TEAM_LEAD" && (
                            <Link
                              href={`/projects/${project.id}/delete`}
                              className="btn btn-sm"
                            >
                              <TrashIcon className="h-4" />
                            </Link>
                          )}
                        </div>
                      </td>
                    </tr>
                  );
                })}
            </tbody>
          </table>
        </div>
      </div>
    </Nav>
  );
};

export default User;
