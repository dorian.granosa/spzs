import Link from "next/link";
import type { InferGetServerSidePropsType, NextPage } from "next";
import { useSession } from "next-auth/react";
import { useEffect, useState } from "react";
import { BookOpenIcon, TrashIcon } from "@heroicons/react/24/outline";

import { requireAuth } from "@/common/requireAuth";
import { Nav, Search, UserRoleBadge } from "@/components";
import { trpc } from "@/common/client/trpc";
import { UserRole } from "@prisma/client";

export const getServerSideProps = requireAuth(async (ctx, session) => {
  return { props: { session } };
});

const Users: NextPage = ({
  session,
}: InferGetServerSidePropsType<typeof getServerSideProps>) => {
  const [userFilter, setUserFilter] = useState("");
  const [userRoleFilter, setUserRoleFilter] = useState<UserRole>();

  const query = trpc.user.search.useQuery(
    {
      filter: userFilter,
      role: userRoleFilter,
    },
    {
      enabled: !!session,
    }
  );

  useEffect(() => {
    if (session) query.refetch();
  }, [session]);

  return (
    <Nav currentPath="users" user={session?.user}>
      <main className="m-3">
        <div className="flex flex-row gap-3 mb-5">
          <Search setCallback={setUserFilter} isLoading={query.isLoading} />
          <select
            id="role"
            className="select select-bordered"
            value={userRoleFilter}
            defaultValue=""
            onChange={(e) => {
              setUserRoleFilter(e.target.value as UserRole);
            }}
          >
            <option disabled value="">
              Role
            </option>
            {Object.keys(UserRole).map((status) => {
              return (
                <option key={status} value={status}>
                  {status}
                </option>
              );
            })}
          </select>
          {userRoleFilter && (
            <button
              className="btn btn-ghost"
              onClick={() => {
                const select = document.querySelector(
                  "#role"
                ) as HTMLSelectElement;
                select.selectedIndex = 0;
                setUserRoleFilter(undefined);
              }}
            >
              X
            </button>
          )}
          <div className="flex-grow"></div>
          {session?.user.role === "TEAM_LEAD" && (
            <button className="btn btn-primary">
              <Link href="/signup">Create user</Link>
            </button>
          )}
        </div>
        <div className="overflow-x-auto">
          <table className="table w-full">
            <thead>
              <tr>
                <th className="font-bold text-right w-2">#</th>
                <th className="hidden md:table-cell">First name</th>
                <th>Last name</th>
                <th>Email</th>
                <th>Role</th>
                <th className="hidden md:table-cell">Created</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              {query.data?.result.map((user, index) => {
                return (
                  <tr className="hover" key={user.id}>
                    <td className="font-bold text-right">{index + 1}</td>
                    <td className="hidden md:table-cell">
                      <Link href={`/users/${user.id}`}>{user.firstName}</Link>
                    </td>
                    <td>
                      <Link href={`/users/${user.id}`}>{user.lastName}</Link>
                    </td>
                    <td>
                      <Link href={`/users/${user.id}`}>{user.email}</Link>
                    </td>
                    <td>
                      <UserRoleBadge role={user.role} />
                    </td>
                    <td
                      className="hidden md:table-cell"
                      suppressHydrationWarning
                    >
                      {new Date(user.createdAt).toLocaleString()}
                    </td>
                    <td className="text-right">
                      <div className="btn-group">
                        <Link href={`/users/${user.id}`} className="btn btn-sm">
                          <BookOpenIcon className="h-4" />
                        </Link>
                        {session?.user?.role === "TEAM_LEAD" && (
                          <Link
                            href={`/users/${user.id}/delete`}
                            className="btn btn-sm btn-disabled"
                          >
                            <TrashIcon className="h-4" />
                          </Link>
                        )}
                      </div>
                    </td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        </div>
      </main>
    </Nav>
  );
};

export default Users;
