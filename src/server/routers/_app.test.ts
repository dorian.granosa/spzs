import { createTRPCProxyClient, httpLink } from "@trpc/client";
import fetch from "isomorphic-fetch";
import { prismaMock } from "../../test_utils/singleton";
import { ServerRouter } from "./_app";

const globalAny = global as any;
globalAny.AbortController = AbortController;
globalAny.fetch = fetch;

describe("signUp", () => {
  const trpcClient = createTRPCProxyClient<ServerRouter>({
    links: [
      httpLink({
        url: "http://localhost:3000/api/trpc",
        fetch,
      }),
    ],
    transformer: undefined,
  });

  const randomString = Math.random().toString(36).substring(7);
  const email = `${randomString}@mail.com`;

  it("should create a new user", async () => {
    // Mock the response of Prisma Client
    prismaMock.user.findFirst.mockResolvedValue(null);
    prismaMock.user.create.mockResolvedValue({
      id: 1,
      firstName: "John",
      lastName: "Doe",
      role: "TEAM_LEAD",
      email: email,
      password: "password",
      createdAt: new Date(),
      updatedAt: new Date(),
    });

    const result = await trpcClient.signUp.mutate({
      firstName: "John",
      lastName: "Doe",
      role: "TEAM_LEAD",
      email: email,
      password: "password",
    });

    expect(result.status).toBe(201);
    expect(result.result).toBe(email);
  });

  it("should throw a ServerError if user already exists", async () => {
    // Mock the response of Prisma Client
    prismaMock.user.findFirst.mockResolvedValue({
      id: 1,
      firstName: "John",
      lastName: "Doe",
      role: "TEAM_LEAD",
      email: email,
      password: "password",
      createdAt: new Date(),
      updatedAt: new Date(),
    });

    const result = await trpcClient.signUp.mutate({
      firstName: "John",
      lastName: "Doe",
      role: "TEAM_LEAD",
      email: email,
      password: "password",
    });

    expect(result.status).toBe(409);
    expect(result.message).toBe("User already exists");
  });
});
