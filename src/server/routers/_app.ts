import { signUpSchema } from "@/common/validation/auth";
import { hash } from "argon2";
import { publicProcedure, router } from "../trpc";
import { meetingRouter } from "./meeting";
import { projectRouter } from "./project";
import { taskRouter } from "./task";
import { taskLogRouter } from "./taskLog";
import { userRouter } from "./user";

export const serverRouter = router({
  user: userRouter,
  task: taskRouter,
  project: projectRouter,
  meeting: meetingRouter,
  taskLog: taskLogRouter,

  signUp: publicProcedure
    .input(signUpSchema)
    .mutation(async ({ input, ctx }) => {
      const { firstName, lastName, role, email, password } = input;

      const exists = await ctx.prisma.user.findFirst({
        where: { email },
      });

      if (exists) {
        return {
          status: 409,
          message: "User already exists",
          result: email,
        };
      }

      const hashedPassword = await hash(password);

      const result = await ctx.prisma.user.create({
        data: { firstName, lastName, role, email, password: hashedPassword },
      });

      return {
        status: 201,
        message: "Account created successfully",
        result: result.email,
      };
    }),
});

export type ServerRouter = typeof serverRouter;
