import {
  meetingGetSchema,
  meetingSchema,
  meetingSearchSchema,
} from "@/common/validation/meeting";
import { TaskStatus, UserRole } from "@prisma/client";
import {
  router,
  authProcedure,
  teamleadProcedure,
  publicProcedure,
} from "../trpc";

export const meetingRouter = router({
  search: authProcedure
    .input(meetingSearchSchema)
    .query(async ({ input, ctx }) => {
      const { userId } = input;

      const user = await ctx.prisma.user.findFirst({
        where: {
          id: userId,
        },
        include: {
          meetings: true,
        },
      });

      if (!user) {
        return {
          status: 404,
          message: "User not found",
          result: null,
        };
      }

      return {
        status: 200,
        message: "Meetings fetched successfully",
        result: user.meetings,
      };
    }),

  get: authProcedure.input(meetingGetSchema).query(async ({ input, ctx }) => {
    const { id } = input;

    const meeting = await ctx.prisma.meeting.findFirst({
      where: {
        id,
      },
      include: {
        project: true,
        attendees: true,
      },
    });

    return {
      status: 200,
      message: "Meeting fetched successfully",
      result: meeting,
    };
  }),

  create: authProcedure
    .input(meetingSchema)
    .mutation(async ({ input, ctx }) => {
      const {
        name,
        description,
        projectId,
        startTime,
        duration,
        interval,
        users,
      } = input;

      const meeting = projectId
        ? await ctx.prisma.meeting.create({
            data: {
              name,
              description,
              project: {
                connect: {
                  id: projectId,
                },
              },
              startTime,
              duration,
              repeatingInterval: interval,
              attendees: {
                connect: users.map((user) => ({
                  id: user.id,
                })),
              },
            },
          })
        : await ctx.prisma.meeting.create({
            data: {
              name,
              description,
              startTime,
              duration,
              repeatingInterval: interval,
              attendees: {
                connect: users.map((user) => ({
                  id: user.id,
                })),
              },
            },
          });

      if (!meeting) {
        return {
          status: 500,
          message: "Something went wrong",
        };
      }

      return {
        status: 201,
        message: "Meeting created successfully",
        result: meeting,
      };
    }),
});
