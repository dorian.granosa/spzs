import {
  projectEditSchema,
  projectGetSchema,
  projectSchema,
  projectSearchSchema,
} from "@/common/validation/project";
import { TaskStatus, UserRole } from "@prisma/client";
import {
  router,
  authProcedure,
  teamleadProcedure,
  publicProcedure,
} from "../trpc";

export const projectRouter = router({
  get: authProcedure.input(projectGetSchema).query(async ({ input, ctx }) => {
    const { id, userFilter, userRoleFilter, taskFilter, taskStatusFilter } =
      input;

    // Fetch project from database
    const project = await ctx.prisma.project.findUnique({
      where: {
        id,
      },
      include: {
        users: {
          where: {
            AND: [
              {
                OR: [
                  {
                    firstName: {
                      contains: userFilter,
                      mode: "insensitive",
                    },
                  },
                  {
                    lastName: {
                      contains: userFilter,
                      mode: "insensitive",
                    },
                  },
                  {
                    email: {
                      contains: userFilter,
                      mode: "insensitive",
                    },
                  },
                ],
              },
              {
                role: userRoleFilter as unknown as UserRole,
              },
            ],
          },
          take: 10,
        },
        tasks: {
          where: {
            AND: [
              {
                name: {
                  contains: taskFilter,
                  mode: "insensitive",
                },
              },
              {
                status: taskStatusFilter as unknown as TaskStatus,
              },
            ],
          },
          include: {
            assignedTo: true,
            reported: true,
          },
          orderBy: {
            updatedAt: "desc",
          },
          take: 10,
        },
      },
    });

    if (!project) {
      return {
        status: 404,
        message: "Project not found",
        result: null,
      };
    }

    return {
      status: 200,
      message: "Project fetched successfully",
      result: project,
    };
  }),

  search: authProcedure
    .input(projectSearchSchema)
    .query(async ({ input, ctx }) => {
      const { name } = input;

      // Fetch projects from database containing the name in the search query, sorted by creation date, number of users, top 10
      const projects = await ctx.prisma.project.findMany({
        include: {
          users: true,
          tasks: true,
        },
        where: {
          name: {
            contains: name,
            mode: "insensitive",
          },
        },
        orderBy: {
          users: {
            _count: "desc",
          },
        },
        take: 10,
      });

      return {
        status: 200,
        message: "Projects fetched successfully",
        result: projects,
      };
    }),

  create: teamleadProcedure
    .input(projectSchema)
    .mutation(async ({ input, ctx }) => {
      const { name, users } = input;

      const project = await ctx.prisma.project.create({
        data: {
          name: name,
          users: {
            connect: users.map((user) => ({
              id: user.id,
            })),
          },
        },
        include: {
          users: true,
        },
      });

      if (!project) {
        return {
          status: 500,
          message: "Something went wrong",
        };
      }

      return {
        status: 201,
        message: "Project created successfully",
        result: project,
      };
    }),

  edit: teamleadProcedure
    .input(projectEditSchema)
    .mutation(async ({ input, ctx }) => {
      const { id, name, users } = input;

      const project = await ctx.prisma.project.update({
        where: {
          id,
        },
        data: {
          name: name,
          users: {
            set: users.map((user) => ({
              id: user.id,
            })),
          },
        },
        include: {
          users: true,
        },
      });

      if (!project) {
        return {
          status: 500,
          message: "Something went wrong",
        };
      }

      return {
        status: 201,
        message: "Project edited successfully",
        result: project,
      };
    }),
});
