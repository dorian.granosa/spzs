import {
  getUserTasksSchema,
  taskCreateSchema,
  taskSchema,
} from "@/common/validation/task";
import { TaskStatus } from "@prisma/client";
import { authProcedure, router } from "../trpc";

export const taskRouter = router({
  getUserTasks: authProcedure
    .input(getUserTasksSchema)
    .query(async ({ ctx, input }) => {
      const { projectId } = input;
      const userId = ctx.session?.user.id;

      const tasks = await ctx.prisma.task.findMany({
        where: {
          projectId: projectId,
          OR: [
            {
              assignedToId: userId,
            },
            {
              reportedId: userId,
            },
          ],
        },
      });

      return tasks;
    }),

  create: authProcedure
    .input(taskCreateSchema)
    .mutation(async ({ ctx, input }) => {
      const task = await ctx.prisma.task.create({
        data: {
          name: input.name,
          description: input.description,
          status: input.status as unknown as TaskStatus,
          assignedToId: input.assignedTo,
          reportedId: input.reported,
          projectId: input.project,
        },
      });

      return task;
    }),

  edit: authProcedure.input(taskSchema).mutation(async ({ ctx, input }) => {
    const task = await ctx.prisma.task.update({
      where: {
        id: input.id,
      },
      data: {
        name: input.name,
        description: input.description,
        status: input.status as unknown as TaskStatus,
        assignedToId: input.assignedTo,
        reportedId: input.reported,
        projectId: input.project,
      },
    });

    return task;
  }),
});
