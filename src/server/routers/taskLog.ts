import { taskLogGetSchema, taskLogSchema } from "@/common/validation/taskLog";
import { authProcedure, router } from "../trpc";

export const taskLogRouter = router({
  get: authProcedure.input(taskLogGetSchema).query(async ({ ctx, input }) => {
    const { date } = input;
    const userId = ctx.session!.user.id;

    const parsedDate = new Date(date);
    parsedDate.setHours(0, 0, 0, 0);

    const taskLogs = await ctx.prisma.taskLog.findMany({
      where: {
        date: parsedDate,
        userId: userId,
      },
      include: {
        task: {
          include: {
            project: true,
          },
        },
      },
    });

    return taskLogs;
  }),

  update: authProcedure
    .input(taskLogSchema)
    .mutation(async ({ ctx, input }) => {
      const { date, taskId, duration } = input;
      const userId = ctx.session!.user.id;

      const parsedDate = new Date(date);
      parsedDate.setHours(0, 0, 0, 0);

      const taskLog = await ctx.prisma.taskLog.upsert({
        where: {
          user_task_date_unique: {
            date: parsedDate,
            taskId: taskId,
            userId: userId,
          },
        },

        update: {
          duration: duration,
        },

        create: {
          date: parsedDate,
          duration: duration,
          task: {
            connect: {
              id: taskId,
            },
          },
          user: {
            connect: {
              id: userId,
            },
          },
        },
      });

      return taskLog;
    }),

  delete: authProcedure
    .input(taskLogSchema)
    .mutation(async ({ ctx, input }) => {
      const { date, taskId } = input;
      const userId = ctx.session!.user.id;

      const parsedDate = new Date(date);
      parsedDate.setHours(0, 0, 0, 0);

      const taskLog = await ctx.prisma.taskLog.delete({
        where: {
          user_task_date_unique: {
            date: parsedDate,
            taskId: taskId,
            userId: userId,
          },
        },
      });

      return taskLog;
    }),
});
