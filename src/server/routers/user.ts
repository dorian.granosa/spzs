import { userGetSchema, userSearchSchema } from "@/common/validation/user";
import { TaskStatus, UserRole } from "@prisma/client";
import {
  router,
  authProcedure,
  teamleadProcedure,
  publicProcedure,
} from "../trpc";

export const userRouter = router({
  get: authProcedure.input(userGetSchema).query(async ({ input, ctx }) => {
    const { id, taskFilter, taskStatusFilter, projectFilter } = input;

    // Fetch User from database
    const user = await ctx.prisma.user.findUnique({
      where: {
        id,
      },
      include: {
        assignedTo: {
          where: {
            AND: [
              {
                name: {
                  contains: taskFilter,
                  mode: "insensitive",
                },
              },
              {
                status: taskStatusFilter as unknown as TaskStatus,
              },
            ],
          },
          include: {
            assignedTo: true,
            reported: true,
          },
          take: 10,
        },
        projects: {
          where: {
            name: {
              contains: projectFilter,
              mode: "insensitive",
            },
          },
          include: {
            users: true,
            tasks: true,
          },
          take: 10,
        },
      },
    });

    return {
      status: 200,
      message: "User fetched successfully",
      result: user,
    };
  }),

  search: authProcedure
    .input(userSearchSchema)
    .query(async ({ input, ctx }) => {
      const { filter, role } = input;

      // Fetch Users from database containing the name (concat firstName and lastName) in the search query or email, sorted by creation date, top 10
      const users = await ctx.prisma.user.findMany({
        where: {
          AND: [
            {
              OR: [
                {
                  firstName: {
                    contains: filter,
                    mode: "insensitive",
                  },
                },
                {
                  lastName: {
                    contains: filter,
                    mode: "insensitive",
                  },
                },
                {
                  email: {
                    contains: filter,
                    mode: "insensitive",
                  },
                },
              ],
            },
            {
              role: role as unknown as UserRole,
            },
          ],
        },
        orderBy: {
          createdAt: "desc",
        },
        take: 10,
      });

      return {
        status: 200,
        message: "Users fetched successfully",
        result: users,
      };
    }),
});
