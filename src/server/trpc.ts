import { initTRPC, TRPCError } from "@trpc/server";
import { Context } from "./context";

const t = initTRPC.context<Context>().create();

export const router = t.router;
export const middleware = t.middleware;
export const publicProcedure = t.procedure;

const isAuthed = t.middleware(({ next, ctx }) => {
  const user = ctx.session?.user;

  if (!user?.email) {
    throw new TRPCError({ code: "UNAUTHORIZED" });
  }

  return next({
    ctx: {
      user,
    },
  });
});

const isTeamlead = isAuthed.unstable_pipe(async ({ next, ctx }) => {
  const user = ctx.user;

  if (user.role != "TEAM_LEAD") {
    throw new TRPCError({ code: "FORBIDDEN" });
  }

  return next({
    ctx: {},
  });
});

export const authProcedure = publicProcedure.use(isAuthed);
export const teamleadProcedure = authProcedure.use(isTeamlead);
