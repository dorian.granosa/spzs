/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./src/pages/**/*.{js,ts,jsx,tsx}",
    "./src/components/**/*.{js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {
      colors: {
        "lavender-purple": "#BD93F9",
        "bright-pink": "#FF79C6",
        "mint-green": "#50FA7B",
        "pale-yellow": "#F1FA8C",
        "baby-blue": "#8BE9FD",
      },
    },
  },
  plugins: [require("daisyui")],
  daisyui: {
    themes: true,
    darkTheme: "dracula",
  },
};
