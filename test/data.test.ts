import { PrismaClient } from "@prisma/client";

const prisma = new PrismaClient();

describe("Fetch users", () => {
  it("should return a list of users", async () => {
    const users = await prisma.user.findMany();
    expect(users.length).toBeGreaterThan(0);
  });

  it("should return a user by id", async () => {
    const user = await prisma.user.findFirst({
      where: {
        id: 1,
      },
    });
    expect(user).toBeDefined();
  });

  it("should return a user by email", async () => {
    const user = await prisma.user.findFirst({
      where: {
        email: "developer@mail.com",
      },
    });
    expect(user).toBeDefined();
  });

  it("should be able to create a new user", async () => {
    const user = await prisma.user.create({
      data: {
        firstName: "John",
        lastName: "Doe",
        email: "new_user@test.com",
        password: "password",
        role: "TEAM_LEAD",
      },
    });
    expect(user).toBeDefined();
  });
});
